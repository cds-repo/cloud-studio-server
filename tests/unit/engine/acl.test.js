/* global describe, it, before, beforeEach, after, afterEach */
"use strict";
const assert = require('assert');

const ACL = require('../../../engine/acl');
const permissions = require('../../mockup/permissions');

let acl;

describe('Engine.ACL unit test', () => {
  before((done) => {
    acl = new ACL();
    done();
  });
  beforeEach((done) => {
    acl.reset();
    done();
  });

  it('should not have elements', (done) => {
    assert.equal(acl.permissions.size(), 0);
    assert.equal(acl.access_table.size(), 0);
    done();
  });

  it('should load permissions', (done) => {
    assert.equal(acl.permissions.size(), 0);
    assert.equal(acl.access_table.size(), 0);

    acl.load(permissions);

    assert.equal(acl.permissions.size(), 2);
    assert.equal(acl.access_table.size(), 0);

    assert.equal(acl.permissions.get('acl.test'), 0);
    assert.equal(acl.permissions.get('acl.notAllowed'), 1);

    done();
  });

  it('should be reset', (done) => {
    assert.equal(acl.permissions.size(), 0);
    assert.equal(acl.access_table.size(), 0);
    done();
  });

  it('should load permissions and modules');
  it('should trigger error when not allowed', (done) => {
    assert.equal(acl.permissions.size(), 0);
    assert.equal(acl.access_table.size(), 0);

    acl.load(permissions);
    assert.equal(acl.permissions.size(), 2);
    assert.equal(acl.access_table.size(), 0);

    assert.equal(acl.permissions.get('acl.test'), 0);
    assert.equal(acl.permissions.get('acl.notAllowed'), 1);

    assert.throws(() => {
      acl.allowed("acl.test");
    }, acl.PermissionError);

    done();
  });
});
