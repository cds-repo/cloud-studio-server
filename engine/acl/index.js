"use strict";
const _ = require('lodash');

class PermissionError extends Error {
  constructor() {
    super("You are not allowed to perform this action!");
  }
}

const ACL = function ACL(permissions, table) {
  const self = this;
  const hidden = {};

  hidden.permissions = {};
  hidden.table = {};

  if (_.isObject(permissions)) {
    hidden.permissions = permissions;
  }

  if (_.isObject(table)) {
    hidden.table = _.cloneDeep(table);
  }

  self.load = function load(newPermissions) {
    hidden.permissions = newPermissions;
  };

  self.permit = function permit(permission) {
    self.access_table.set(permission, self.permissions.get(permission));
  };

  self.allowed = function allowed(permission) {
    const ownPermission = self.access_table.get(permission);
    let existsPermission = self.permissions.get(permission);
    existsPermission = existsPermission !== undefined && existsPermission !== null;

    if (!_.isBoolean(ownPermission)) {
      throw new PermissionError();
    }

    if (ownPermission !== existsPermission) {
      throw new PermissionError();
    }

    return true;
  };

  self.has = function has(permission) {
    const ownPermission = self.access_table.get(permission);
    let existsPermission = self.permissions.get(permission);
    existsPermission = existsPermission !== undefined && existsPermission !== null;

    if (!_.isBoolean(ownPermission)) {
      return false;
    }

    if (ownPermission !== existsPermission) {
      return false;
    }

    return true;
  };

  self.permissions = {
    size: () => Object.keys(hidden.permissions).length,
    get: (permission) => {
      if (!_.isString(permission)) {
        return hidden.permissions;
      }

      return hidden.permissions[permission];
    },
    set: (name, value) => {
      hidden.permissions[name] = value;
    }
  };

  self.access_table = {
    size: () => Object.keys(hidden.table).length,
    get: (permission) => {
      if (!_.isString(permission)) {
        return hidden.table;
      }

      return _.filter(hidden.table, (userPerm) => userPerm.permission === permission).length > 0;
    },
    set: (name, value) => {
      hidden.table[name] = value;
    }
  };

  self.reset = function reset() {
    hidden.table = {};
    hidden.permissions = {};
  };

  self.PermissionError = PermissionError;

  return self;
};

module.exports = ACL;
