"use strict";

const path = require('path');
const fs = require('fs');
const _ = require('lodash');

const Routes = function Routes(engine) {
  const self = this;
  self.engine = engine;

  self.register = function register(root, routes) {
    if (!routes) {
      routes = 'routes';
    }

    const fullpath = path.join(root, routes);
    fs.readdirSync(fullpath).forEach((file) => {
      if (path.extname(file) === '.js') {
        require(_.join([fullpath, file], '/'))(self.engine.app, self.engine);
      }
    });
  };

  self.build = function build(root, routes) {
    if (!routes) {
      routes = 'routes';
    }

    const fullpath = path.join(root, routes);
    fs.readdirSync(fullpath).forEach((file) => {
      if (path.extname(file) === '.js') {
        const classPath = require(_.join([fullpath, file], '/'));
        const classRoute = new classPath(self.engine);
        classRoute.build();
      }
    });
  };

  return self;
};


module.exports = function ModuleRoute(engine) {
  return new Routes(engine);
};
