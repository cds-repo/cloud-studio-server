"use strict";
const _ = require('lodash');

class PermissionException extends Error {
  constructor(permission) {
    super(`The given permission [${permission}] already exists.`);
  }
}

class DuplicatePermissionException extends Error {
  constructor(permission, value) {
    super(`The given value [${value}] for the permission [${permission}] already exists for another permission.`);
  }
}

class Permission {
  constructor() {
    this.permissions = {};
  }

  push(permission, value) {
    if (!_.isNull(this.permissions[permission]) && !_.isUndefined(this.permissions[permission])) {
      throw new PermissionException(permission);
    }
    if (_.values(this.permissions).indexOf(value) >= 0) {
      throw new DuplicatePermissionException(permission, value);
    }

    this.permissions[permission] = value;
  }

  import(json) {
    const self = this;
    _.each(json, (v, k) => {
      self.push(k, v);
    });
  }

  get(permission) {
    return this.permissions[permission];
  }

  all() {
    return this.permissions;
  }
}

module.exports = Permission;
