"use strict";

module.exports = {
  routes: require("./routes"),
  API: require("./API"),
  Modules: require("./modules")
};
