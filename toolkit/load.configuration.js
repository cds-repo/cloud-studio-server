"use strict";

const db = require("../config/db");
const r = require("rethinkdb");

const permissions = {
  user: require('../defaults/user.perm'),
  lead: require('../defaults/lead.perm'),
  manager: require('../defaults/manager.perm')
};

const CoreApp = function CoreApp() {
  const self = {};

  self.run = function run() {
    const perms = [];

    perms.push({
      key: "cds.permissions.default.user",
      value: JSON.stringify(permissions.user)
    });
    perms.push({
      key: "cds.permissions.default.lead",
      value: JSON.stringify(permissions.lead)
    });
    perms.push({
      key: "cds.permissions.default.manager",
      value: JSON.stringify(permissions.manager)
    });

    r.connect({
      host: db.host,
      port: db.port,
      db: db.db
    })
      .then((conn) => {
        r.table("Configurations")
          .filter((config) => config("key").eq("cds.permissions.default.user")
            .or(config("key").eq("cds.permissions.default.lead"))
            .or(config("key").eq("cds.permissions.default.manager")))
          .delete()
          .run(conn)
          .then(() => r.table("Configurations")
            .insert(perms)
            .run(conn)
            .then((cursor) => {
              console.log("Loaded permissions");
              process.exit(0);
            }))
          .catch((err) => {
            console.err(err);
            process.exit(1);
          });
      });
  };

  return self;
};

module.exports = CoreApp;
