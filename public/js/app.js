const CDSApp = (function CDSApp() {
  if (!this instanceof CDSApp)
    return new CDSApp();

  const self = this;

  self.styleDisplay = function styleDisplay() {
    let height = $(window).height();
    console.log(height);

    if ($(".cds-app-header").length != 0) {
      height -= $(".cds-app-header").height() + parseInt($(".cds-app-header").css('margin-bottom')) + 8;
      console.log(height);
      $(".cds-app-mainblock").height(height);
    }
  };

  self.sortableTable = function sortableTable() {
    let table = $(".cds-datatables");
    let options = {};

    if (table.has(".cds-actions"))
      options = {
        "columnDefs": [
          {"orderable": false, "targets": -1}
        ]
      };

    table.DataTable(options);
  };

  self.apiPath = function apiPath(path, id) {
    let apiPath = '/api/v1/' + path;

    if (id)
      apiPath += '/' + id;

    return apiPath.replace('\/\/', '/');
  };

  self.errorFunction = function errorFunction(error) {
    let jsonResponse = error.responseJSON;
    // for (let i in jsonResponse) {
    //   const field = jsonResponse[i];
    //   const parentBlock = $("#fld" + _.upperFirst(field.field)).parent();
    //   const childBlock = parentBlock.children('.help-block');
    //
    //   childBlock.text(field.message);
    //   childBlock.show();
    //   parentBlock.addClass('has-error');
    // }
    $(".cds-errors").text(jsonResponse.message);
    $(".cds-errors").removeClass("hidden");
  };

  window.refreshScreen = function refreshScreen() {
    $(location).attr('href', $(location).attr('href'));
  };

  self.successFunction = function (data) {
    console.log(data);
  };

  self.CDSReq = new (function CDSReq() {
    var self = this,
      hidden = {};

    hidden.request = function (url, type, data, success, error) {
      $.ajax({
        data: data,
        dataType: 'json',
        method: type.toUpperCase(),
        url: url,

        error: error,
        success: success
      });
    };

    self.get = function (url, success, error) {
      if (!error)
        error = success;

      hidden.request(url, "GET", {}, success, error);
    };
    self.post = function (url, data, success, error) {
      if (!error)
        error = success;

      // $.get('/csrfToken', function (csrfData) {
      //   data = _.extend(data, csrfData);
      hidden.request(url, "POST", data, success, error);
      // });
    };
    self.put = function (url, data, success, error) {
      if (!error)
        error = success;

      // $.get('/csrfToken', function (csrfData) {
      //   data = _.extend(data, csrfData);
      hidden.request(url, "PUT", data, success, error);
      // });
    };
    self.delete = function (url, data, success, error) {
      if (!error)
        error = success;

      // $.get('/csrfToken', function (csrfData) {
      //   data = _.extend(data, csrfData);
      hidden.request(url, "DELETE", data, success, error);
      // });
    };

    return self;
  })();

  $(document).on("submit", ".cds-form", function (e) {
    e.preventDefault();

    var data = $(this).serializeObject(),
      type = $(this).attr('method'),
      path = $(this).data('api-path'),
      id = $(this).data('api-id'),
      callback = window[$(this).data('api-callback')] || self.successFunction;

    $(".cds-form .help-block").removeClass('has-error');
    $(".cds-form .help-block").text('');
    $(".cds-errors").addClass("hidden");

    _.each(data, function dataChange(v, k) {
      if (_.isEmpty(v))
        delete data[k];
    });

    $.ajax({
      data: data,
      dataType: 'json',
      method: type,
      url: apiPath(path, id),

      error: self.errorFunction,
      success: callback
    });

    return false;
  });

  $(document).on("click", ".cds-delete", function deleteElement(e) {
    e.preventDefault();

    var path = $(this).data('api-path'),
      id = $(this).data('api-id'),
      elem = $(this).data('api-element');

    var element = elem ? $(`#${elem}-${id}`) : $(`#${path}-${id}`);

    const success = function (data) {
      element.remove();
    };

    self
      .CDSReq
      .delete(apiPath(path, id), {}, success, self.errorFunction);
  });

  self.sortableTable();
  self.styleDisplay();
  const lazyStyleDisplay = _.debounce(styleDisplay, 300);

  $(window).resize(function () {
    lazyStyleDisplay();
  });

  return self;
})();
