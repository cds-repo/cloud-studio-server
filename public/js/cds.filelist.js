var CDSFileList = function (projectId, isAdmin) {
  var table = $("#cds-list-files"),
    tableBody = $("#cds-list-files tbody"),
    header = $("#cds-folderCrumb"),
    codeZone = $("#cds-fileContent"),
    headerHome = $("#cds-pathHome").text(),
    headerTpl = $("#cds-pathTemplate").text(),
    elementTpl = $("#cds-elementTemplate").text(),
    codeTpl = $("#cds-codeTemplate").text();

  var CDSReq = CDSApp.CDSReq;
  var theID = projectId;
  var isProject = true;
  var self = this;

  self.urlBuilder = function (type, path) {
    var suffix = "";

    if (path)
      suffix = "?path=" + path;

    if (isProject) {
      return '/api/v1/project/' + theID + '/' + type + suffix;
    }

    return '/api/v1/clone/' + theID + '/' + type + suffix;
  };

  self.pathBuilder = function (path, type, fileType) {
    var pathExplode = path.substring(1).split('/');

    header.text("");
    header.append(Mustache.render(headerHome));

    var fullPath = '';
    for (var i in pathExplode) {
      if (pathExplode[i] === undefined || pathExplode[i] === null ||
        pathExplode[i].length === 0)
        continue;

      fullPath = fullPath + '/' + pathExplode[i];
      header.append(Mustache.render(headerTpl, {
        name: pathExplode[i],
        fullPath: fullPath
      }));
    }

    var lastElement = header.children(":last");
    lastElement.addClass("active");
    lastElement.html(lastElement.children().html());

    header.data('path', path);
    header.data('type', type);
    header.data('filetype', fileType);
  };

  self.readFolder = function (path) {
    path = path.replace('\/\/', '/');
    table.show();
    codeZone.hide();

    CDSReq.get(self.urlBuilder('listing', path), function (res) {
      self.pathBuilder(path, 'directory', '');

      tableBody.text("");
      for (var i in res) {
        tableBody.append(Mustache.render(elementTpl, _.extend(res[i], {
          parent: path
        })));
      }

      self.displayOwnSettings();
    });
  };

  self.readFile = function (path, type) {
    path = path.replace('\/\/', '/');
    var pathExplode = path.substring(1).split('/');

    CDSReq.get(self.urlBuilder('read', path), function (res) {
      self.pathBuilder(path, 'read', type);

      codeZone.html(Mustache.render(codeTpl, {
        type: type,
        code: res.content.trim()
      }));

      table.hide();
      codeZone.show();

      Prism.highlightAll();
    });
  };

  self.readImage = function (path) {
    path = path.replace('\/\/', '/');

    CDSReq.get(self.urlBuilder('read', path), function (res) {
      self.pathBuilder(path, 'read', '');

      var img = $("<img/>", {
        alt: "image",
        class: "img-responsive",
        src: "data:image/png;base64," + res.content.trim()
      });

      codeZone.text("");
      img.appendTo(codeZone);

      table.hide();
      codeZone.show();
    });
  };

  self.deleteElement = function (parent, path, type) {
    var fullPath = (parent + '/' + path).replace('\/\/', '/');

    CDSReq.delete(self.urlBuilder('delete'), {
      path: fullPath,
      type
    }, function (res) {
      if (res.status)
        if (res.status == "ok")
          return self.readFolder(parent);
        else if (res.status == 500)
          alert(res.responseJSON.message);
    });
  };

  self.displayOwnSettings = function () {
    var own = $("#cds-repository").children(':selected').data('own');
    if (!own && !isAdmin)
      $(".cds-file-delete").remove();

    self.displayMergeSettings(!own && !isAdmin);
  };

  self.displayMergeSettings = function (extraChecks) {
    var val = $("#cds-repository").val().trim();

    if (val === undefined || val === null || val.length === 0)
      return $("#cds-cloneButtons").hide();

    if (extraChecks)
      $("#cds-cloneButtons").hide();
    else
      $("#cds-cloneButtons").css('display', 'inline-block');
  };

  self.displayEditor = function (extraChecks) {
    if(isAdmin){
      $("#cds-openEditor").css('display', 'inline-block');
      return;
    }

    var val = $("#cds-repository").val().trim();

    if (val === undefined || val === null || val.length === 0)
      return $("#cds-openEditor").hide();

    if (extraChecks)
      $("#cds-openEditor").hide();
    else
      $("#cds-openEditor").css('display', 'inline-block');
  };

  $(document).on("click", ".cds-file-delete", function (e) {
    e.preventDefault();

    var type = $(this).data('type'),
      parent = $(this).data('parent'),
      path = $(this).data('path');

    self.deleteElement(parent, path, type);
  });

  $(document).on("click", ".cds-element-link", function (e) {
    e.preventDefault();

    var type = $(this).data('type'),
      parent = $(this).data('parent'),
      path = $(this).data('path'),
      fileType = $(this).data('filetype');

    switch (type) {
      case "directory":
        self.readFolder(parent + '/' + path);
        break;
      case "file":
        self.readFile(parent + '/' + path, fileType);
        break;
      case "image":
        self.readImage(parent + '/' + path);
        break;
    }
  });

  $(document).on("click", "#cds-refreshScreen", function (e) {
    e.preventDefault();

    var type = header.data('type'),
      path = header.data('path'),
      fileType = header.data('filetype');

    switch (type) {
      case "directory":
        self.readFolder(path);
        break;

      case "file":
        self.readFile(path, fileType);
        break;
    }
  });

  $(document).on("click", "#cds-openEditor", function (e) {
    e.preventDefault();

    var editorURL = '';

    if (isProject) {
      editorURL = '/editor/project/' + theID;
    } else {
      editorURL = '/editor/clone/' + theID;
    }

    window.open(editorURL, '_blank');
  });

  $(document).on("click", "#cds-folderCrumb a", function (e) {
    e.preventDefault();

    self.readFolder($(this).data('path'));
  });

  $(document).on("change", "#cds-repository", function (e) {
    var el = $(this).children(':selected');
    var own = el.data('own');

    theID = el.val().trim();
    isProject = false;
    if (theID === null || theID === undefined || theID.length === 0) {
      theID = projectId;
      isProject = true;
    }

    self.displayMergeSettings(!own && !isAdmin);
    self.displayEditor(!own && !isAdmin);

    self.readFolder('/');
  });

  self.displayEditor(false);
  self.readFolder('/');

  return self;
};
