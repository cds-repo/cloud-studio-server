"use strict";

module.exports = {
  secret: 'qwertyuiop',
  store: 'cds.session.db',
  key: 'cds.session.sid',
  table: 'Sessions'
};
