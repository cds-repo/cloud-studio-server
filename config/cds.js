module.exports = {
  language: 'en',
  contact: 'support@cloud-studio.ro',
  issues: 'http://issues.cloud-studio.ro',
  docs: 'http://docs.cloud-studio.ro'
};
