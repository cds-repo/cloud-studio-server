"use strict";
const API = require("../../../engine/core/API");
const _ = require("lodash");
const Projectinfo = require("../../../package.json");

class BootstrapConfig extends API {
  constructor(engine) {
    super(engine, "runtime");
  }

  register() {
    this.router.get('/', (req, res) => this.getConfig(this, req, res));
  }


  getConfig(self, req, res) {
    self.allowed(req.user, 'getConfig', 'csc.core.bootstrap.getConfig');
    const config = {};

    self._version(config);
    self._api(config);
    self._runtime(config);
    config.id = req.query.id;
    config.type = req.query.type;

    res.set('Content-Type', 'application/javascript');
    res.render('bootstrap/bootstrap', _.extend(config, {
      layout: null
    }));
  }

  _version(config) {
    config.version = this.engine.version.version;
  }

  _runtime(config) {
    config.runtime = this.engine.app.runtime;

    const http = this.engine.config.http;
    if (http.port != 80 && http.port != 443) {
      config.path = `http://${this.engine.config.http.host}:${this.engine.config.http.port}/`;
    } else if (http.port == 80) {
      config.path = `http://${this.engine.config.http.host}/`;
    } else if (http.port == 443) {
      config.path = `https://${this.engine.config.http.host}/`;
    }

    config.server = `${config.path}${config.api}`;
  }

  _api(config) {
    config.name = this.engine.config.engine.name;
    config.api = "api/v1";
    config.cds = this.engine.config.cds;
  }

}

module.exports = BootstrapConfig;
