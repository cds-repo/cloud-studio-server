"use strict";
const Route = require("../../../engine/core/Route");

class EditorNotAllowed extends Error {

}

class Editor extends Route {
  constructor(engine) {
    super(engine, 'editor');
  }

  register() {
    this.router.get('/project/:id', (req, res) => this.getProjectEditor(this, req, res));
    this.router.get('/clone/:id', (req, res) => this.getCloneEditor(this, req, res));
  }

  _render(type, req, res) {
    const Project = this.engine.models.Project;
    const Clone = this.engine.models.Clone;

    let whatToSearch;

    if (type === "project") {
      whatToSearch = Project.get(req.params.id);
    } else {
      whatToSearch = Clone.get(req.params.id).getJoin({
        project: true
      });
    }

    whatToSearch
      .run()
      .then((result) => {
        if (!req.user.isAdmin()) {
          switch (type) {
            case "project":
              if (result.ownerID !== req.user.id) {
                throw new EditorNotAllowed("You cannot edit this project!");
              }
              break;
            case "clone":
              if (result.userID !== req.user.id && result.project.ownerID !== req.user.id) {
                throw new EditorNotAllowed("You cannot edit this clone!");
              }
              break;
          }
        }

        res.render('editor', {
          user: req.user,
          id: result.id,
          type,
          layout: null
        });
      })
      .catch((e) => res.throw(e));
  }

  getProjectEditor(self, req, res) {
    self.redirectUnauthenticated(req.user, res);
    self.allowed(req.user, 'getProjectEditor', 'csc.core.editor.getProjectEditor');

    self._render('project', req, res);
  }

  getCloneEditor(self, req, res) {
    self.redirectUnauthenticated(req.user, res);
    self.allowed(req.user, 'getCloneEditor', 'csc.core.editor.getProjectEditor');

    self._render('clone', req, res);
  }
}

module.exports = Editor;
