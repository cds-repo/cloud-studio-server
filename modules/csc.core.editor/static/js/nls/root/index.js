define({
  /* File Menu Items */
  "FILE": "File",
  "FILE_NEW": "New file",
  "FILE_NEW_WINDOW": "New window",
  "FILE_OPEN": "Open file",
  "FILE_CLOSE": "Close file",
  "FILE_SAVE": "Save file",
  "FILE_SAVE_ALL": "Save all",
  "FILE_SAVE_AS": "Save as...",
  "FILE_LOGOUT": "Logout",
  "FILE_EXIT": "Exit",

  /* Edit Menu Items */
  "EDIT": "Edit",
  "EDIT_UNDO": "Undo",
  "EDIT_REDO": "Redo",
  "EDIT_CUT": "Cut",
  "EDIT_COPY": "Copy",
  "EDIT_PASTE": "Paste",
  "EDIT_FIND": "Find",
  "EDIT_FIND_NEXT": "Find next",
  "EDIT_FIND_PREV": "Find prev",
  "EDIT_REPLACE": "Replace",
  "EDIT_GOTO": "Jump to line",

  /* Project Menu Items */
  "PROJECT": "Project",
  "PROJECT_DEBUG": "Debug",
  "PROJECT_DEPLOY": "Deploy",
  "PROJECT_DOWNLOAD": "Download",
  "PROJECT_RUN": "Run",
  "PROJECT_INFO": "Project Info",
  "PROJECT_SETTINGS": "Project Settings",

  /* Help Menu Items */
  "HELP": "Help",
  "HELP_HOWTO": "How to use",
  "HELP_ISSUE": "Report an issue",
  "HELP_ACCOUNT": "My Account",
  "HELP_ABOUT": "About",

  /* Extra Toolbar */
  "HOME": "Home",
  "PROFILE": "Profile",
  "LOGOUT": "Logout",

  /* Sidebar Elements */
  "PROJECT-FILES": "Files",
  "TOOLBOX": "Toolbox",
  "PROPERTIES": "Properties",

  /*###### Labels & Messages ######*/
  /*Global Utils*/
  "YES": "Yes",
  "NO": "No",
  "OK": "Okay",
  "CANCEL": "Cancel",
  "SAVE": "Save",
  "CREATE": "Create",

  /*Message Utils*/
  "ERROR_TITLE": "ERROR",
  "NOTIFICATION_TITLE": "Notification",
  "PROMPT_TITLE": "Prompt",

  /*Action Messages*/
  "OPEN_A_FILE": "Open a file first!",
  "DOCS_NOTIFICATION": "You will be redirected to the application documentation.",
  "BROWSER_RESTRICTED": "Currently this buttons are not supported, but you can use the coresponding shortcuts.",
  "ISSUES_NOTIFICATION": "You will be redirected to the application issue tracker.",
  "UNSAVED_NOTIFICATION": "There are some unsaved files.\n\nAre you sure you want to exit?",
  "SAVED": "Fil saved",

  /*Project Files Messages*/
  "NEWFILE_LABEL": "Give the file name:",
  "NEWFOLDER_LABEL": "Give the folder name:",
  "RENAME_LABEL": "Give the new file name:",
  "NOFILE": "No file given!!!",
  "DELETE_CONFIRMATION": "Are you sure you want to delete the file '%s'?",

  "CTX_NEWFILE": "New file",
  "CTX_NEWFOLDER": "New folder",
  "CTX_NEWWINDOW": "New window",
  "CTX_RENAME": "Rename",
  "CTX_DELETE": "Delete",
  "CTX_REFRESH": "Refresh file tree",

  "CTX2_DELETEOBJ": "Delete object",
  "CTX2_PROPERTIES": "Object properties",

  "SAMPLE_MENU": "Sample",
  "SAMPLE_BUTTON": "Sample Button",
  "SAMPLE_MESSAGE": "This is a sample button"
});
