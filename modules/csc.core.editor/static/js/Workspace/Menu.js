define(function (require, exports, module) {
  "use strict";

  const View = require("Base/View");
  const MainMenu = require("Menu/index");
  const Language = require("language");
  const Shortcut = require("Utils/Shortcut");

  class Menu extends View {
    constructor() {
      super("Views/Menu/view.html");

      this.buttonTpl = require('text!Views/Menu/button.html');
      this.buttonShortcutTpl = require('text!Views/Menu/button2.html');
      this.dividerTpl = require('text!Views/Menu/divider.html');
      this.dropdownTpl = require('text!Views/Menu/dropdown.html');
    }

    _renderButton(element) {
      const _options = element.forge(Language[element.get('name')]);
      return Mustache.render(this.buttonTpl, _options);
    }

    _renderDropDown(element) {
      const _options = element.forge(Language[element.get('name')]);
      return Mustache.render(this.dropdownTpl, _options);
    }

    _renderButtonDropDown(element) {
      const _options = element.forge(Language[element.get('name')]);
      return Mustache.render(this.buttonShortcutTpl, _options);
    }

    _renderDivider(element) {
      return Mustache.render(this.dividerTpl);
    }

    render() {
      require([`text!${this._view}`], (view) => {
        this._dashboard.append(Mustache.render(view, Language));

        _.each(MainMenu.menu, (v) => {
          const menu = v.menu;
          Shortcut.add(menu.definition.get('name'), menu.definition.get('shortcut'));

          if (menu.definition.get('type') === 'button') {
            $("#topbar #menubar").append(this._renderButton(menu.definition));
          }

          if (menu.definition.get('type') === 'dropdown') {
            $("#topbar #menubar").append(this._renderDropDown(menu.definition));

            const _submenu = menu.definition.get('submenu');
            const _id = menu.definition.get('name');

            for (let elementID = 0; elementID < _submenu.length; elementID++) {
              Shortcut.add(_submenu[elementID].get('name'), _submenu[elementID].get('shortcut'));

              if (_submenu[elementID].get('type') === 'button')
                $("#" + _id + " ul").append(this._renderButtonDropDown(_submenu[elementID]));

              if (_submenu[elementID].get('type') === 'divider')
                $("#" + _id + " ul").append(this._renderDivider(_submenu[elementID]));
            }
          }
        });
      });
    }
  }

  module.exports = Menu;
});
