define(function (require, exports, module) {
  "use strict";

  const View = require("Base/View");

  class Modal extends View {
    constructor() {
      super("Views/modal.html");
    }
  }

  module.exports = Modal;
});
