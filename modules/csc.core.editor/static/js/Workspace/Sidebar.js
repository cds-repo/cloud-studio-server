define(function (require, exports, module) {
  "use strict";

  const View = require("Base/View");
  const Language = require("language");
  const WindowHookup = require("Utils/WindowHookup");
  const ActionHookup = require("Utils/ActionHookup");
  const MainSidebar = require("Sidebar/index");
  const UIEvents = require("Utils/UIEvents");

  class Sidebar extends View {
    constructor() {
      super("Views/Sidebar/view.html");

      this.containerTpl = require('text!Views/Sidebar/default.html');
    }

    init() {
      ActionHookup.init('closeSidebar');
      ActionHookup.init('clickSidebar');

      WindowHookup.add("onReady", () => {
        $("#mainbar a:not(.disabled,.active)").tooltip({placement: 'right'});
        $("#mainbar a.disabled").attr('title', '');
        this._resizeWindow();
      });

      WindowHookup.add("onResize", this._resizeWindow);
    }

    _resizeWindow() {
      $("#sidebar").css({height: window.innerHeight - $(".dashboard #topbar").height() - 1});
      $("#sidebar-ext").css({height: window.innerHeight - $(".dashboard #topbar").height() - 1});
      $(".widget .content").css({height: $(".widget").height() - $(".widget .header").outerHeight() - 1});
    }

    render() {
      require([`text!${this._view}`], (view) => {
        this._dashboard.append(Mustache.render(view, Language));

        _.each(MainSidebar.sidebar, (v) => {
          const sidebar = v.sidebar;

          const _options = sidebar.forge(Language[v.sidebar.id]);
          $("#sidebar-ext").append(Mustache.render(this.containerTpl, _options));

          sidebar.refill();
        });

        this.initEvents();
      });
    }

    initEvents() {
      UIEvents.click("#sidebar li a", this._sidebarClick);
      UIEvents.click(".close-widget", this._closeSidebar);
    }

    _closeSidebar() {
      $("#sidebar-ext").toggle('slide', 'left');
      $("#sidebar-ext").removeClass('online');
      $("#sidebar-ext .active").removeClass('active');
      $("#sidebar li a").removeClass("active");

      ActionHookup.execute('closeSidebar');
    }

    _sidebarClick(e) {
      if ($(this).hasClass("disabled")) {
        return;
      }

      const is_visible = $("#sidebar-ext").css('display') !== "none";
      const active_tab = $("#sidebar-ext .active").attr('id');

      if (active_tab !== undefined) {
        if (active_tab === $(this).data('widget')) {
          return;
        }
      }

      $(".contentLoader").show();
      if (is_visible) {
        $("#sidebar-ext").removeClass('online');
        $("#sidebar-ext .active").removeClass('active');
        $("#sidebar li a").removeClass("active");
      }

      const widgetName = $(this).data('widget');
      $(`#sidebar-ext #${widgetName}`).addClass('active');
      $("#sidebar-ext").addClass('online');
      if (!is_visible) {
        $("#sidebar-ext").toggle('slide', 'left');
      }
      $(this).addClass("active");

      $(".widget .content").css({height: $(".widget").height() - $(".widget .header").outerHeight() - 1});
      ActionHookup.execute('clickSidebar');
      $(".contentLoader").hide();
    }
  }

  module.exports = Sidebar;
});
