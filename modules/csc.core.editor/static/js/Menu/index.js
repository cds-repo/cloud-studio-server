define(function (require, exports, module) {
  "use strict";

  const FileMenu = require("Menu/File/index");
  const EditMenu = require("Menu/Edit/index");
  const ProjectMenu = require("Menu/Project/index");
  const HelpMenu = require("Menu/Help/index");

  class MenuWrapper {
    constructor() {
      this._sidebars = [];
    }

    add(name, menu) {
      this._sidebars.push({name, menu});
    }

    addAfter(index, name, menu) {
      if (_.isNumber(index)) {
        this._sidebars.splice(index, 0, {name, menu});
        return;
      }

      let ix = -1;

      _.each(this._sidebars, (v, k) => {
        if (v.name === index) {
          ix = k;
          return;
        }
      });

      if (ix < 0) {
        this._sidebars.push({name, menu})
      } else {
        this._sidebars.splice(ix + 1, 0, {name, menu});
      }
    }

    get menu() {
      return this._sidebars;
    }
  }

  const menuWrapper = new MenuWrapper();
  menuWrapper.add('File', new FileMenu());
  menuWrapper.add('Edit', new EditMenu());
  menuWrapper.add('Project', new ProjectMenu());
  menuWrapper.add('Help', new HelpMenu());

  module.exports = menuWrapper;
});
