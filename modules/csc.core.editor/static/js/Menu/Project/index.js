define(function (require, exports, module) {
  "use strict";

  const Menu = require("Base/Menu");
  const UIEvents = require("Utils/UIEvents");

  class Project extends Menu.Menu {
    constructor() {
      super("Menu/Project");
    }

    initEvents() {
      UIEvents.click("#PROJECT_RUN");
      UIEvents.click("#PROJECT_DEPLOY");
      UIEvents.click("#PROJECT_DOWNLOAD");
      UIEvents.click("#PROJECT_INFO");
      UIEvents.click("#PROJECT_SETTINGS");
    }
  }

  module.exports = Project;
});
