define(function (require, exports, module) {
  "use strict";

  const Menu = require("Base/Menu");
  const UIEvents = require("Utils/UIEvents");
  const Message = require("Base/Message");
  const Language = require("language");
  const Modal = require("Base/Modal");

  class Help extends Menu.Menu {
    constructor() {
      super("Menu/Help");
    }

    initEvents() {
      UIEvents.click("#HELP_HOWTO", this._howTo);
      UIEvents.click("#HELP_ISSUE", this._issue);
      UIEvents.click("#HELP_ABOUT", this._about);
    }

    _howTo() {
      Message
        .Confirm(
          Language.DOCS_NOTIFICATION,
          () => {
            var win = window.open(CDSConfig.Docs, "_blank");
            win.focus();
          }
        )
        .prepare(Modal.OK_CANCEL)
        .show();
    }

    _issue() {
      Message
        .Confirm(
          Language.ISSUES_NOTIFICATION,
          () => {
            var win = window.open(CDSConfig.Issues, "_blank");
            win.focus();
          }
        )
        .prepare(Modal.OK_CANCEL)
        .show();
    }

    _about() {
      var text = `${CDSConfig.Name} ${CDSConfig.Version}<br />`;
      text += `2016 &copy; Valentin Duricu <${CDSConfig.Contact}>`;

      Message
        .Notification(text, "About")
        .prepare(Modal.OK_CANCEL)
        .show();
    }
  }

  module.exports = Help;
});
