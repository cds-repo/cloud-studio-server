define(function (require, exports, module) {
  "use strict";

  const Language = require("language");
  const Modal = require("Base/Modal");

  const modalID = "#modal_one";
  const Messages = {};
  const _buildModal = (modal, message, title, callback) => {
    modal.title = title;
    modal.text = message;
    modal.action = callback;
    modal.make();
  };

  Messages.Notification = (message, title) => Messages.Confirm(message, null, title);

  Messages.Confirm = (message, callback, title) => {
    if (message === undefined || message === null || message === "") {
      return;
    }

    const confirmModal = new Modal(modalID, Modal.OK_CANCEL);

    if (title === undefined || title === null || title === "") {
      title = Language.NOTIFICATION_TITLE;
    }

    _buildModal(confirmModal, message, title, callback);
    return confirmModal;
  };

  Messages.Prompt = (message, value, callback, title) => {
    if (message === undefined || message === null || message === "") {
      return;
    }

    const promptModal = new Modal(modalID, Modal.OK_CANCEL);

    if (title === undefined || title === null || title === "") {
      title = Language.PROMPT_TITLE;
    }

    message = `${message}<br /><br /><input type="text" id="prompt-value" class="form-control" value="${value}">`;

    _buildModal(promptModal, message, title, () => {
      const val = $("#prompt-value").val();
      if (callback != undefined && callback != null) {
        callback(val);
      } else {
        console.log(val);
      }
    });
    return promptModal;
  };

  Messages.Error = (message, title) => {
    if (message === undefined || message === null || message === "") {
      return;
    }

    const errorModal = new Modal(modalID, Modal.OK_CANCEL);
    if (title === undefined || title === null || title === "") {
      title = Language.ERROR_TITLE;
    }


    _buildModal(errorModal, message, title, null);
    errorModal.afterShow = () => {
      throw message;
    };

    return errorModal;
  };

  Messages.FormCall = (message, title, action, callback) => {
    if (message == undefined || message == null || message == "") {
      return;
    }

    const formCallModal = new Modal(modalID, Modal.SAVE_CANCEL);

    if (title == undefined || title == null || title == "") {
      title = Language.NOTIFICATION_TITLE;
    }

    _buildModal(formCallModal, message, title, callback);

    formCallModal.yesButton.attr("disabled", true);
    action(formCallModal.yesButton, formCallModal.bodyTxt);

    return formCallModal;
  };

  module.exports = Messages;
});
