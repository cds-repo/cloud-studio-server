define(function (require, exports, module) {
  "use strict";

  const Language = require("language");

  class View {
    constructor(viewFile, inhibitMustache) {
      this._view = viewFile;
      this._data = {};
      this._dashboard = $(".dashboard");

      this._inhibitMustache = inhibitMustache || false;

      this.init();
    }

    init() {

    }

    data(data) {
      this._data = data;
    }

    push(element, value) {
      this._data[element] = value;
    }

    render() {
      require([`text!${this._view}`], (view) => {
        let data = this._data;
        data = _.extend(data, Language);
        if (this._inhibitMustache) {
          this._dashboard.append(view);
        } else {
          this._dashboard.append(Mustache.render(view, data));
        }
      });
    }
  }

  module.exports = View;
});
