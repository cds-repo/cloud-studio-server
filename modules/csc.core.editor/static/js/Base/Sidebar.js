define(function (require, exports, module) {
  "use strict";

  class Sidebar {
    constructor(id) {
      this.id = id;
    }

    forge(name) {
      return {
        id: this.id,
        name
      }
    }

    fill(callback) {
      callback();
    }

    refill() {
      if ($(".contentLoader").css('display') != "block") {
        $(".contentLoader").show();
      }

      this.fill(() => {
        $(".contentLoader").hide();
      });
    }
  }

  module.exports = Sidebar;
});
