define(function (require, exports, module) {
  "use strict";

  class Render {
    constructor() {
      this._views = [];
    }

    pushView(name, view) {
      this._views.push({name, view});
    }

    render() {
      _.each(this._views, (v)=> {
        v.view.render();
      });
    }

    get(name) {
      let theView = _.filter(this._views, (view) => view.name === name);

      if (theView.length === 0) {
        return null;
      }

      return theView.pop();
    }
  }

  const renderObject = new Render();

  module.exports = renderObject;
});
