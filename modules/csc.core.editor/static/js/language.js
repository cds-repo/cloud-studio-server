define(function (require, exports, module) {
  "use strict";

  const Language = require("i18n!nls/index");

  module.exports = Language;
});
