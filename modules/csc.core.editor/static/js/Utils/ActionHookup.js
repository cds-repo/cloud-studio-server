define(function (require, exports, module) {
  "use strict";

  const Hookup = require("Utils/Hookup");
  const ActionHookup = new Hookup();

  module.exports = ActionHookup;
});
