define(function (require, exports, module) {
  "use strict";

  class Shortcut {
    constructor() {
      this._shortkeys = {
        _ctrl: [],
        _alt: [],
        _shift: [],
        _ctrl_shift: [],
        _ctrl_alt: [],
        _ctrl_alt_shift: [],
        _alt_shift: [],
        _none: []
      };

      this.initEvents();
    }

    get shortcut() {
      return _.cloneDeep(this._shortkeys);
    }

    initEvents() {
      $(document).on("keydown", (e) => {
        if (!this.ifXCV(e) && this.acceptedKey(e)) {
          e.preventDefault();

          let element = "";
          element += e.ctrlKey ? "_ctrl" : "";
          element += e.altKey ? "_alt" : "";
          element += e.shiftKey ? "_shift" : "";

          if (element == "") {
            element = "_none";
          }

          let key = "";
          if (e.which >= 65 && e.which <= 90) {
            key = String.fromCharCode(e.which);
            $("#" + this._shortkeys[element][key]).trigger("click");
          } else if (e.which >= 112 && e.which <= 123) {
            key = "F" + Math.abs(e.which - 111);
            $("#" + this._shortkeys[element][key]).trigger("click");
          }
        }
      });
    }

    add(id, keyCombination) {
      if (keyCombination === undefined || keyCombination === null || keyCombination === "") {
        return;
      }

      const keySplit = keyCombination.split("+");
      let ctrl = false;
      let shift = false;
      let alt = false;

      for (let i = 0; i < keySplit.length; i++) {
        const key = keySplit[i].toLowerCase();
        switch (key) {
          case "ctrl":
            ctrl = true;
            break;
          case "alt":
            alt = true;
            break;
          case "shift":
            shift = true;
            break;
        }
      }
      let element = "";
      element += ctrl ? "_ctrl" : "";
      element += alt ? "_alt" : "";
      element += shift ? "_shift" : "";

      if (element == "") {
        element = "_none";
      }

      let key = keySplit.pop().toUpperCase();
      this._shortkeys[element][key] = id;
    }

    ifXCV(e) {
      if (e.ctrlKey) {
        switch (String.fromCharCode(e.which)) {
          case 'X':
          case 'C':
          case 'V':
            return true;
        }
      }

      return false;
    }

    acceptedKey(e) {
      if (e.which >= 112 && e.which <= 123) {
        return true;
      }

      if (e.ctrlKey || e.altKey) {
        return true;
      }

      return false;
    }

  }

  const shortcut = new Shortcut();

  module.exports = shortcut;
});
