define(function (require, exports, module) {
  "use strict";

  class Hookup {
    constructor(eventsArray) {
      this._events = {};
      if (_.isObject(eventsArray)) {
        this._events = eventsArray;
      }
    }

    init(event) {
      this._events[event] = [];
    }

    add(event, callback) {
      if (!this._events[event])
        this._events[event] = [];

      this._events[event].push(callback);
    }

    remove(event, element) {
      this._events[event].splice(element - 1, 1);
    }

    get(event) {
      return this._events[event];
    }

    pop(event) {
      this._events[event].pop();
    }

    execute(event) {
      for (let action in this._events[event]) {
        this._events[event][action]();
      }
    }
  }

  module.exports = Hookup;
});
