"use strict";

const TeamView = function UserView(engine) {
  const self = {};

  engine.routes.build(__dirname);
  engine.views.registerView(__dirname);
  engine.permissions.import(require('./permissions.json'));

  return self;
};


module.exports = TeamView;
