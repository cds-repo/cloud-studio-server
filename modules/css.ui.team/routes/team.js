"use strict";
const Route = require("../../../engine/core/Route");
const _ = require('lodash');

class Team extends Route {
  constructor(engine) {
    super(engine, 'teams');
  }

  register() {
    this.router.get('/', (req, res) => this.getList(this, req, res));
    this.router.get('/create', (req, res) => this.getNew(this, req, res));
    this.router.get('/:id', (req, res) => this.getView(this, req, res));
    this.router.get('/:id/edit', (req, res) => this.getEdit(this, req, res));
    this.router.get('/:id/projects', (req, res) => this.getProjects(this, req, res));
  }

  getList(self, req, res) {
    self.allowedOrRedirect(req.user, "css.ui.team.list", res);

    const TeamRepo = self.engine.modules.get("css.app.models.api").TeamRepository;

    TeamRepo
      .getAllJoin({
        lead: true,
        members: true
      })
      .then((teams) => res.render('team/index', {
        user: req.user,
        teams
      }))
      .catch((e) => res.throw(e));
  }

  getView(self, req, res) {
    self.allowedOrRedirect(req.user, "css.ui.team.view", res);

    const TeamRepo = self.engine.modules.get("css.app.models.api").TeamRepository;
    const User = self.engine.modules.get("css.app.models.api").UserRepository;

    const result = {
      user: req.user
    };

    TeamRepo
      .getJoin(req.params.id, {
        lead: true,
        members: true
      })
      .then((team) => {
        result.team = team;

        return User.getAll();
      })
      .then((users) => {
        result.users = _.filter(users,
          (user) => _.filter(result.team.members,
            (member) => member.id === user.id).length === 0);

        res.render('team/view', result);
      })
      .catch((e) => res.throw(e));
  }

  getEdit(self, req, res) {
    self.allowedOrRedirect(req.user, "css.ui.team.edit", res);

    const TeamRepo = self.engine.modules.get("css.app.models.api").TeamRepository;
    const User = self.engine.modules.get("css.app.models.api").UserRepository;
    const ranks = self.engine.models.UserRank;

    const result = {
      user: req.user
    };

    TeamRepo
      .get(req.params.id)
      .then((team) => {
        result.team = team;

        return User.filter((user) => user("rank").ge(ranks.LEAD));
      })
      .then((users) => {
        result.users = users;
        res.render('team/edit', result);
      })
      .catch((e) => res.throw(e));
  }

  getNew(self, req, res) {
    self.allowedOrRedirect(req.user, "css.ui.team.new", res);

    const TeamModel = self.engine.models.Team;
    const User = self.engine.modules.get("css.app.models.api").UserRepository;
    const ranks = self.engine.models.UserRank;

    User
      .filter((user) => user("rank").ge(ranks.LEAD))
      .then((users) => res.render('team/edit', {
        user: req.user,
        team: new TeamModel({}),
        users
      }))
      .catch((e) => res.throw(e));
  }

  getProjects(self, req, res) {
    self.allowedOrRedirect(req.user, "css.ui.team.projects", res);

    const Project = self.engine.modules.get("css.app.models.api").ProjectRepository;

    Project
      .filterJoin({
        teamID: req.params.id
      }, {
        type: true,
        owner: true,
        clones: true
      })
      .then((projects) => res.render('project/index', {
        user: req.user,
        projects
      }))
      .catch((e) => res.throw(e));
  }

}

module.exports = Team;
