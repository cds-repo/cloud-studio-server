"use strict";
const Route = require("../../../engine/core/Route");
const _ = require("lodash");
let r;

class Project extends Route {
  constructor(engine) {
    super(engine, 'projects');
    r = engine.app.r;
  }

  register() {
    this.router.get('/', (req, res) => this.getList(this, req, res));
    this.router.get('/all', (req, res) => this.getListAll(this, req, res));
    this.router.get('/create', (req, res) => this.getNew(this, req, res));
    this.router.get('/:id', (req, res) => this.getView(this, req, res));
    this.router.get('/:id/info', (req, res) => this.getInfo(this, req, res));
    this.router.get('/:id/contributors', (req, res) => this.getContributors(this, req, res));
    this.router.get('/:id/files', (req, res) => this.getFiles(this, req, res));
    this.router.get('/:id/edit', (req, res) => this.getEdit(this, req, res));
  }

  _renderView(self, view, pageSpecific, req, res) {
    const ProjectRepo = self.engine.modules.get("css.app.models.api").ProjectRepository;
    const Clone = self.engine.modules.get("css.app.models.api").CloneRepository;
    const ProjectType = self.engine.modules.get("css.app.models.api").ProjectTypeRepository;
    const Team = self.engine.modules.get("css.app.models.api").TeamRepository;
    const Activity = self.engine.models.Activity;
    const User = self.engine.models.User;

    const id = req.params.id;
    const projectJoin = {
      type: true,
      owner: true,
      team: true,
      clones: true
    };

    ProjectRepo
      .getJoin(id, projectJoin)
      .then((data) => {
        let result = _.extend({
          user: req.user
        }, {
          project: data
        });

        result = _.extend(result, pageSpecific);

        if (pageSpecific.informationActive) {
          res.render(view, result);
          return;
        }

        if (pageSpecific.contributorsActive || pageSpecific.sourcesActive) {
          Clone
            .filterJoin({
              projectID: data.id
            }, {
              user: true
            })
            .then((clones) => {
              result = _.extend(result, {
                clones
              });

              return User.run();
            })
            .then((users) => {
              users = _.filter(users,
                (user) => _.filter(result.clones,
                  (clone) => clone.user.id === user.id).length === 0);
              result = _.extend(result, {
                users
              });

              res.render(view, result);
            });
        }

        if (pageSpecific.dashboardActive) {
          Activity
            .filter({
              projectID: data.id
            })
            .getJoin({
              project: true,
              user: true
            })
            .orderBy(r.desc('createdAt'))
            .run()
            .then((activities) => {
              result = _.extend(result, {
                activities
              });

              res.render(view, result);
            });
        }

        if (pageSpecific.editActive) {
          ProjectType
            .getAll()
            .then((types) => {
              result = _.extend(result, {
                types
              });

              return Team.getAll();
            })
            .then((teams) => {
              result = _.extend(result, {
                teams
              });

              return User.run();
            })
            .then((users) => {
              result = _.extend(result, {
                users
              });
              res.render(view, result);
            });
        }
      })
      .catch((err) => res.throw(err));
  }

  getView(self, req, res) {
    self.allowedOrRedirect(req.user, "css.ui.project.view", res);

    self._renderView(self, 'project/view', {
      dashboardActive: true
    }, req, res);
  }

  getInfo(self, req, res) {
    self.allowedOrRedirect(req.user, "css.ui.project.info", res);

    self._renderView(self, 'project/info', {
      informationActive: true
    }, req, res);
  }

  getContributors(self, req, res) {
    self.allowedOrRedirect(req.user, "css.ui.project.contributors", res);

    self._renderView(self, 'project/contributors', {
      contributorsActive: true
    }, req, res);
  }

  getFiles(self, req, res) {
    self.allowedOrRedirect(req.user, "css.ui.project.files", res);

    self._renderView(self, 'project/sources', {
      sourcesActive: true
    }, req, res);
  }

  getEdit(self, req, res) {
    self.allowedOrRedirect(req.user, "css.ui.project.edit", res);

    self._renderView(self, 'project/edit', {
      editActive: true
    }, req, res);
  }

  getNew(self, req, res) {
    self.allowedOrRedirect(req.user, "css.ui.project.new", res);
    const ProjectModel = self.engine.models.Project;
    const ProjectType = self.engine.modules.get("css.app.models.api").ProjectTypeRepository;

    ProjectType
      .getAll()
      .then((types) => {
        res.render('project/create', {
          user: req.user,
          project: new ProjectModel({}),
          types
        });
      });
  }

  getList(self, req, res) {
    self.allowedOrRedirect(req.user, "css.ui.project.list", res);

    const ProjectModel = self.engine.models.Project;

    const data = {
      user: req.user,
      canClone: true
    };

    ProjectModel
      .filter({
        restricted: false
      })
      .getJoin({
        type: true,
        owner: true,
        team: true,
        clones: true
      })
      .orderBy('createdAt')
      .run()
      .then((projects) => res.render('project/index', _.extend(data, {
        projects
      })))
      .catch((e) => res.throw(e));
  }

  getListAll(self, req, res) {
    self.allowedOrRedirect(req.user, "css.ui.project.listAll", res);

    const ProjectModel = self.engine.models.Project;

    const data = {
      user: req.user
    };

    ProjectModel
      .getJoin({
        type: true,
        owner: true,
        team: true,
        clones: true
      })
      .orderBy('createdAt')
      .run()
      .then((projects) => res.render('project/index', _.extend(data, {
        projects
      })))
      .catch((e) => res.throw(e));
  }
}

module.exports = Project;
