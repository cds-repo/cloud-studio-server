"use strict";
const Validator = require("../Validator");
const ValidationException = require("../ValidationException");

class ActiveUserValidator extends Validator {
  validate(model, data, when, done, error) {
    switch (when) {
      case "create":
      case "createJoin":
        this.model
          .filter((activeUser) => activeUser("userID").eq(model.userID))
          .then((activeUsers) => {
            if (activeUsers.length !== 0) {
              throw new ValidationException("You cannot have a duplicate active user. Use the update function.");
            }

            done(model);
          })
          .catch((e) => error(e));
        break;
      case 'update':
      case 'updateJoin':
        this.model
          .filter((activeUser) => activeUser("userID").eq(model.userID).and(activeUser("id").ne(model.id)))
          .then((activeUsers) => {
            if (activeUsers.length !== 0) {
              throw new ValidationException("You cannot have a duplicate active user. Use the update function.");
            }

            done(model);
          })
          .catch((e) => error(e));
        break;
      default:
        done(model);
    }
  }
}

module.exports = ActiveUserValidator;
