"use strict";
const Validator = require("../Validator");
const ValidationException = require("../ValidationException");

class CloneValidator extends Validator {
  validate(model, data, when, done, error) {
    switch (when) {
      case 'create':
      case 'createJoin':
      case 'update':
      case 'updateJoin':
      default:
        this.model
          .filter((clone) => clone("userID").eq(model.userID)
            .and(clone("projectID").eq(model.projectID)))
          .then((clones) => {
            if (clones.length !== 0) {
              throw new ValidationException("You cannot have a duplicate clone.");
            }

            done(model);
          })
          .catch((e) => error(e));
        /* TODO: Validate docker port. */
        done(model);
    }
  }
}

module.exports = CloneValidator;
