"use strict";
const Validator = require("../Validator");
// const ValidationException = require("../ValidationException");
const slug = require("slug");

class ProjectValidator extends Validator {
  validate(model, data, when, done, error) {
    switch (when) {
      case "create":
      case "createJoin":
        model.slug = slug(model.name);
        done(model);
        // this.model
        //   .filter((project) => project("slug").eq(model.slug))
        //   .then((projects) => {
        //     if (projects.length !== 0) {
        //       throw new ValidationException("You cannot have a duplicate project slug.");
        //     }
        //
        //     done(model);
        //   })
        //   .catch((e) => error(e));
        break;
      case 'update':
      case 'updateJoin':
        // this.model
        //   .filter((project) => project("slug").eq(model.slug).and(project("id").ne(model.id)))
        //   .then((projects) => {
        //     if (projects.length !== 0) {
        //       throw new ValidationException("You cannot have a duplicate project slug.");
        //     }
        //
        //     done(model);
        //   })
        //   .catch((e) => error(e));
        // break;
        done(model);
        break;
      default:
        /* TODO: Validate docker port. */
        done(model);
    }
  }
}

module.exports = ProjectValidator;
