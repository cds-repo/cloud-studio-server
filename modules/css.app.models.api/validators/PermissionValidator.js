"use strict";
const Validator = require("../Validator");
const ValidationException = require("../ValidationException");

class PermissionValidator extends Validator {
  validate(model, data, when, done, error) {
    switch (when) {
      case "create":
      case "createJoin":
        this.model
          .filter((permission) => permission("userID").eq(model.userID)
            .and(permission("permission").eq(model.permission)))
          .then((permissions) => {
            if (permissions.length !== 0) {
              throw new ValidationException("You cannot have a duplicate permission element.");
            }

            done(model);
          })
          .catch((e) => error(e));
        break;
      case 'update':
      case 'updateJoin':
        this.model
          .filter((permission) => permission("userID").eq(model.userID)
            .and(permission("permission").eq(model.permission))
            .and(permission("id").ne(model.id)))
          .then((permissions) => {
            if (permissions.length !== 0) {
              throw new ValidationException("You cannot have a duplicate permission element.");
            }

            done(model);
          })
          .catch((e) => error(e));
        break;
      default:
        done(model);
    }
  }
}

module.exports = PermissionValidator;
