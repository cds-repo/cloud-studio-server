"use strict";
const Validator = require("../Validator");
const ValidationException = require('../ValidationException');
const _ = require("lodash");

class UserValidator extends Validator {
  validate(model, data, when, done, error) {
    const self = this;
    switch (when) {
      case "create":
      case "createJoin":
        this.model
          .filter((user) => user("username").eq(model.username).or(user("email").eq(model.email)))
          .then((users) => {
            if (users.length !== 0) {
              throw new ValidationException("You cannot have a duplicate user");
            }

            self._validatePassword(model, data);

            done(model);
          })
          .catch((e) => error(e));
        break;

      case "update":
      case "updateJoin":
        this.model
          .filter((user) => user("username").eq(model.username)
            .or(user("email").eq(model.email))
            .and(user("id").ne(model.id)))
          .then((users) => {
            if (users.length !== 0) {
              throw new ValidationException("You cannot have a duplicate user");
            }

            if (!_.isEmpty(data.password)) {
              self._validatePassword(model, data);
            }

            done(model);
          })
          .catch((e) => error(e));
        break;

      default:
        done(model);
    }
  }

  _validatePassword(model, data) {
    if (data.password.length < 6) {
      throw new ValidationException("Password must be at least 6 characters long");
    }

    if (!_.isString(data.confirmPassword)) {
      throw new ValidationException("You need a password confirmation");
    }

    if (data.confirmPassword !== data.password) {
      throw new ValidationException("The 2 passwords must be the same");
    }
  }
}

module.exports = UserValidator;
