"use strict";
const Validator = require("../Validator");
const ValidationException = require("../ValidationException");

class ConfigurationValidator extends Validator {
  validate(model, data, when, done, error) {
    switch (when) {
      case "create":
      case "createJoin":
        this.model
          .filter((configuration) => configuration("key").eq(model.key))
          .then((configurations) => {
            if (configurations.length !== 0) {
              throw new ValidationException("You cannot have a duplicate configuration element.");
            }

            done(model);
          })
          .catch((e) => error(e));
        break;
      case 'update':
      case 'updateJoin':
        this.model
          .filter((configuration) => configuration("key").eq(model.key).and(configuration("id").ne(model.id)))
          .then((configurations) => {
            if (configurations.length !== 0) {
              throw new ValidationException("You cannot have a duplicate configuration element.");
            }

            done(model);
          })
          .catch((e) => error(e));
        break;
      default:
        done(model);
    }
  }
}

module.exports = ConfigurationValidator;
