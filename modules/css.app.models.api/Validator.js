"use strict";

class Validator {
  constructor(model) {
    this.model = model;
  }

  validate(model, data, when, done, error) {
    throw new Error("NotImplemented");
  }
}

module.exports = Validator;
