"use strict";

class RepositoryException extends Error {
  constructor(model) {
    super(`Cannot find the model [${model}]`);
  }
}

module.exports = RepositoryException;
