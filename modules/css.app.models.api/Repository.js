"use strict";
const _ = require("lodash");
const RepositoryException = require("./RepositoryException");
const ValidationException = require("./ValidationException");

class Repository {
  constructor(model) {
    this.model = model;
    this._validator = null;
  }

  get validator() {
    return this._validator;
  }

  set validator(validatorClass) {
    this._validator = new validatorClass(this.model);
  }

  validate(model, data, when) {
    const self = this;
    return new Promise((resolve, reject) => {
      if (!_.isObject(self.validator)) {
        resolve(model);
        return;
      }

      try {
        // const validator = new self.validator(self.model);
        self._validator.validate(model, data, when, resolve, reject);
      } catch (e) {
        reject(e);
      }
    });
  }

  enrich(model, data, when) {
    return new Promise((resolve, reject) => {
      resolve(model);
    });
  }

  create(data) {
    const newModel = new this.model(data);
    const self = this;

    return this.validate(newModel, data, "create")
      .then((modelValidated) => self.enrich(modelValidated, data, "create"))
      .then((modelEnriched) => modelEnriched.save())
      .catch((e) => {
        if (e instanceof ValidationException) {
          throw e;
        }
        if (e.name === "ValidationError") {
          throw new ValidationException(e.message, 500);
        }
        throw new RepositoryException(self.model.getTableName());
      });
  }

  createJoin(data, join) {
    const self = this;
    const newModel = new this.model(data);

    return this.validate(newModel, data, "createJoin")
      .then((modelValidated) => self.enrich(modelValidated, data, "createJoin"))
      .then((modelEnriched) => modelEnriched.saveAll(join))
      .catch((e) => {
        if (e instanceof ValidationException) {
          throw e;
        }
        if (e.name === "ValidationError") {
          throw new ValidationException(e.message, 500);
        }
        throw new RepositoryException(self.model.getTableName());
      });
  }

  update(id, data) {
    const self = this;

    return this.model
      .get(id)
      .run()
      .then((model) => {
        if (!model) {
          throw new RepositoryException(self.model.getTableName());
        }
        model = _.extend(model, data);

        return self.validate(model, data, "update")
          .then((modelValidated) => self.enrich(modelValidated, data, "update"))
          .then((modelEnriched) => modelEnriched.save());
      })
      .catch((e) => {
        if (e instanceof ValidationException) {
          throw e;
        }
        if (e.name === "ValidationError") {
          throw new ValidationException(e.message, 500);
        }
        throw new RepositoryException(self.model.getTableName());
      });
  }

  updateJoin(id, data, join) {
    const self = this;

    return this.model
      .get(id)
      .getJoin(join)
      .run()
      .then((model) => {
        if (!model) {
          throw new RepositoryException(self.model.getTableName());
        }
        model = _.extend(model, data);

        return self.validate(model, data, "updateJoin")
          .then((modelValidated) => self.enrich(modelValidated, data, "updateJoin"))
          .then((modelEnriched) => modelEnriched.saveAll(join));
      })
      .catch((e) => {
        if (e instanceof ValidationException) {
          throw e;
        }
        if (e.name === "ValidationError") {
          throw new ValidationException(e.message, 500);
        }
        throw new RepositoryException(self.model.getTableName());
      });
  }

  remove(id) {
    const self = this;

    return self.model
      .get(id)
      .run()
      .then((model) => {
        if (!model) {
          throw new RepositoryException(self.model.getTableName());
        }

        return model.delete();
      })
      .catch((e) => {
        if (e instanceof ValidationException) {
          throw e;
        }
        if (e.name === "ValidationError") {
          throw new ValidationException(e.message, 500);
        }
        throw new RepositoryException(self.model.getTableName());
      });
  }

  removeJoin(id, join) {
    const self = this;

    return this.model
      .get(id)
      .getJoin(join)
      .run()
      .then((model) => {
        if (!model) {
          throw new RepositoryException(self.model.getTableName());
        }

        return model.deleteAll(join);
      })
      .catch((e) => {
        if (e instanceof ValidationException) {
          throw e;
        }
        throw new RepositoryException(self.model.getTableName());
      });
  }

  getAll() {
    return this.model
      .run();
  }

  get(id) {
    return this.model
      .get(id)
      .run();
  }

  getAllJoin(join) {
    return this.model
      .getJoin(join)
      .run();
  }

  getJoin(id, join) {
    return this.model
      .get(id)
      .getJoin(join)
      .run();
  }

  filter(filter) {
    return this.model
      .filter(filter)
      .run();
  }

  filterJoin(filter, join) {
    return this.model
      .filter(filter)
      .getJoin(join)
      .run();
  }
}

module.exports = Repository;
