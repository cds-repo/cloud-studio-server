"use strict";
const Repository = require("../Repository");

class ActivityRepository extends Repository {
  constructor(engine) {
    super(engine.models.Activity);
  }
}

module.exports = ActivityRepository;
