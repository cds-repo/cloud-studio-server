"use strict";
const Repository = require("../Repository");

class TeamRepository extends Repository {
  constructor(engine) {
    super(engine.models.Team);
  }
}

module.exports = TeamRepository;
