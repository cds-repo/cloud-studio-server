"use strict";
const Repository = require("../Repository");

class ProjectRepository extends Repository {
  constructor(engine) {
    super(engine.models.Project);
    this.validator = require('../validators/ProjectValidator');
  }
}

module.exports = ProjectRepository;
