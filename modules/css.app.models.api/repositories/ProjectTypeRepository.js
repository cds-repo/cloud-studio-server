"use strict";
const Repository = require("../Repository");

class ProjectTypeRepository extends Repository {
  constructor(engine) {
    super(engine.models.ProjectType);
  }
}

module.exports = ProjectTypeRepository;
