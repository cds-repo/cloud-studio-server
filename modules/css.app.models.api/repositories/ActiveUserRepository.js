"use strict";
const Repository = require("../Repository");

class ActiveUserRepository extends Repository {
  constructor(engine) {
    super(engine.models.ActiveUser);
    this.validator = require('../validators/ActiveUserValidator');
  }
}

module.exports = ActiveUserRepository;
