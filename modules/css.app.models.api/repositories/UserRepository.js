"use strict";
const Repository = require('../Repository');
const Promise = require('bluebird');
const bcrypt = require('bcryptjs');
const _ = require('lodash');

class UserRepository extends Repository {
  constructor(engine) {
    super(engine.models.User);
    this.validator = require('../validators/UserValidator');
  }

  enrich(model, data, when) {
    return new Promise((resolve, reject) => {
      switch (when) {
        case "create":
        case "createJoin":
          model.password = bcrypt.hashSync(data.password);

          resolve(model);
          break;

        case "update":
        case "updateJoin":
          if (!_.isEmpty(data.password)) {
            model.password = bcrypt.hashSync(data.password);
          }

          resolve(model);
          break;
        default:
          resolve(model);
      }
    });
  }
}

module.exports = UserRepository;
