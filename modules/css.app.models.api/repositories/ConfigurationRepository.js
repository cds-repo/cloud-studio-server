"use strict";
const Repository = require("../Repository");

class ConfigurationRepository extends Repository {
  constructor(engine) {
    super(engine.models.Configuration);
    this.validator = require('../validators/ConfigurationValidator');
  }
}

module.exports = ConfigurationRepository;
