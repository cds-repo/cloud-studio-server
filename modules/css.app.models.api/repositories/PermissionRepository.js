"use strict";
const Repository = require("../Repository");

class PermissionRepository extends Repository {
  constructor(engine) {
    super(engine.models.Permission);
    this.validator = require('../validators/PermissionValidator');
  }
}

module.exports = PermissionRepository;
