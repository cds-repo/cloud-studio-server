"use strict";
const Repository = require("../Repository");

class CloneRepository extends Repository {
  constructor(engine) {
    super(engine.models.Clone);
    this.validator = require('../validators/CloneValidator');
  }
}

module.exports = CloneRepository;
