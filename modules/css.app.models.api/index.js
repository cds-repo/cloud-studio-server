"use strict";
const _ = require("lodash");
const fs = require("fs");
const path = require("path");

const ModelsAPI = function ModelsAPI(engine) {
  const self = {
    Exceptions: {
      ValidationException: require('./ValidationException'),
      RepositoryException: require('./RepositoryException')
    }
  };

  const fullPath = _.join([__dirname, "repositories"], '/');

  fs.readdirSync(fullPath).forEach((file) => {
    if (path.extname(file) === '.js') {
      const classPath = require(_.join([fullPath, file], '/'));
      self[path.basename(file, '.js')] = new classPath(engine);
    }
  });
  return _.extend(self, this);
};


module.exports = ModelsAPI;
