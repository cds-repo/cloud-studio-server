"use strict";

class ValidationException extends Error {
  constructor(message, httpCode) {
    super(message);
    this.httpCode = httpCode || 500;
  }
}

module.exports = ValidationException;
