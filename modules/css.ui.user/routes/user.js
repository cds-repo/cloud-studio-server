"use strict";
const Route = require("../../../engine/core/Route");
const _ = require('lodash');

class User extends Route {
  constructor(engine) {
    super(engine, 'users');
  }

  register() {
    this.router.get('/', (req, res) => this.getList(this, req, res));
    this.router.get('/create', (req, res) => this.getNew(this, req, res));
    this.router.get('/:id', (req, res) => this.getView(this, req, res));
    this.router.get('/:id/edit', (req, res) => this.getEdit(this, req, res));
    this.router.get('/:id/projects', (req, res) => this.getProjects(this, req, res));
    this.router.get('/:id/clones', (req, res) => this.getClones(this, req, res));
    this.router.get('/:id/permissions', (req, res) => this.getPermissions(this, req, res));
  }

  getList(self, req, res) {
    self.allowedOrRedirect(req.user, "css.ui.user.list", res);

    const UserRepo = self.engine.modules.get("css.app.models.api").UserRepository;

    UserRepo
      .getAllJoin({
        ownProjects: true,
        clones: true
      })
      .then((users) => res.render('users/index', {
        user: req.user,
        users
      }))
      .catch((e) => res.throw(e));
  }

  getView(self, req, res) {
    self.allowedOrRedirect(req.user, "css.ui.user.view", res);
    const UserRepo = self.engine.modules.get("css.app.models.api").UserRepository;

    UserRepo
      .getJoin(req.params.id, {
        permissions: true
      })
      .then((userX) => {
        res.render('users/view', {
          user: req.user,
          userX,
        });
      })
      .catch((e) => res.throw(e));
  }

  getEdit(self, req, res) {
    self.allowedOrRedirect(req.user, "css.ui.user.edit", res);
    const UserRepo = self.engine.modules.get("css.app.models.api").UserRepository;
    const ranks = self.engine.models.UserRank;

    UserRepo
      .get(req.params.id)
      .then((userX) => {
        res.render('users/edit', {
          user: req.user,
          userX,
          ranks
        });
      })
      .catch((e) => res.throw(e));
  }

  getNew(self, req, res) {
    self.allowedOrRedirect(req.user, "css.ui.user.new", res);
    const UserModel = self.engine.models.User;
    const ranks = self.engine.models.UserRank;

    res.render('users/edit', {
      user: req.user,
      userX: new UserModel({}),
      ranks
    });
  }

  getProjects(self, req, res) {
    self.allowedOrRedirect(req.user, "css.ui.user.projects", res);

    const Project = self.engine.modules.get("css.app.models.api").ProjectRepository;

    Project
      .filterJoin({
        ownerID: req.params.id
      }, {
        type: true,
        owner: true,
        clones: true
      })
      .then((projects) => res.render('project/index', {
        user: req.user,
        projects
      }))
      .catch((e) => res.throw(e));
  }

  getClones(self, req, res) {
    self.allowedOrRedirect(req.user, "css.ui.user.clones", res);

    const Project = self.engine.models.Project;
    const Clone = self.engine.models.Clone;

    Clone
      .filter({
        userID: req.params.id
      })
      .run()
      .then((clones) => {
        const projectIds = [];

        _.each(clones, (v) => {
          projectIds.push(v.projectID);
        });

        if (projectIds.length) {
          const result = Project.getAll.apply(Project, projectIds);
          return result
            .getJoin({
              type: true,
              owner: true,
              team: true,
              clones: true
            })
            .orderBy('createdAt')
            .run();
        }
        return [];
      })
      .then((projects) => res.render('project/index', {
        clones: true,
        user: req.user,
        projects
      }))
      .catch((e) => res.throw(e));
  }

  getPermissions(self, req, res) {
    self.allowedOrRedirect(req.user, "css.ui.user.permissions", res);
    const UserRepo = self.engine.modules.get("css.app.models.api").UserRepository;


    UserRepo
      .getJoin(req.params.id, {
        permissions: true
      })
      .then((userX) => {
        res.render('users/permissions', {
          user: req.user,
          userX
        });
      })
      .catch((e) => res.throw(e));
  }

}

module.exports = User;
