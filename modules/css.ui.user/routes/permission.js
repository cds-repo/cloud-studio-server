"use strict";
const Route = require("../../../engine/core/Route");
const _ = require('lodash');

class Permission extends Route {
  constructor(engine) {
    super(engine, 'permissions');
  }

  register() {
    this.router.get('/', (req, res) => this.getList(this, req, res));
  }

  getList(self, req, res) {
    self.allowedOrRedirect(req.user, "css.ui.permission.view", res);

    res.render('permissions/index', {
      user: req.user,
      permissions: self._parsePermissions()
    });
  }

  _parsePermissions() {
    const permissions = this.engine.permissions.all();
    let parsed = [];

    _.each(permissions, (v, k) => {
      let obj = k.split(".");

      let buildUp = "";

      for (let i = 3; i < obj.length; i++) {
        buildUp = `${buildUp}.${obj[i]}`;
      }
      obj[3] = buildUp.substring(1);
      obj = obj.slice(0,4);

      parsed.push({
        base: obj[0],
        module: obj[1],
        section: obj[2],
        permission: obj[3],
        value: v
      });
    });

    return parsed;
  }

}

module.exports = Permission;
