"use strict";
const Route = require("../../../engine/core/Route");

class Profile extends Route {
  constructor(engine) {
    super(engine, 'profile');
  }

  register() {
    this.router.get('/', (req, res) => this.getProfile(this, req, res));
  }

  getProfile(self, req, res) {
    self.redirectUnauthenticated(req.user, res);

    res.render('profile/index', {
      user: req.user
    });
  }
}

module.exports = Profile;
