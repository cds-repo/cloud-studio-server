"use strict";
const ProjectDescription = require("../../package.json");
const _ = require("lodash");

module.exports = function CoreRuntime(engine) {
  engine.app.runtime = "local";
  engine.config.runtime = "";
  engine.app.editor = true;
};
