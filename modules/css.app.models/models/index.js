"use strict";

module.exports = (thinky) => {
  const models = {
    ActiveUser: require('./activeUser')(thinky),
    Activity: require('./activity')(thinky),
    Configuration: require('./configuration')(thinky),
    Clone: require('./clone')(thinky),
    ProjectType: require('./projectType')(thinky),
    Project: require('./project')(thinky),
    Permission: require('./permission')(thinky),
    Team: require('./team')(thinky),
    User: require('./user')(thinky)
  };

  return models;
};
