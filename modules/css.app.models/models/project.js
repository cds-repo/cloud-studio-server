"use strict";

const ProjectWrapper = (thinky) => {
  const type = thinky.type;

  const Project = thinky.createModel("Projects", {
    id: type.string(),
    name: type.string().required(),
    slug: type.string().required(),
    description: type.string(),
    typeID: type.string(),
    teamID: type.string(),
    ownerID: type.string(),
    restricted: type.boolean().default(false),
    dockerPort: type.number().integer().min(0),
    repository: type.string(),
    repositoryType: type.string().enum(["git", "hg", "local"]).default("local"),
    createdAt: type.date(),
    updatedAt: type.date()
  }, {
    enforce_extra: "remove"
  });

  return Project;
};


module.exports = ProjectWrapper;
