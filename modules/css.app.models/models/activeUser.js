"use strict";

const ActiveUserWrapper = (thinky) => {
  const type = thinky.type;

  const ActiveUser = thinky.createModel("ActiveUsers", {
    id: type.string(),
    userID: type.string(),
    counts: type.number().integer().min(0).default(1),
    createdAt: type.date(),
    updatedAt: type.date()
  }, {
    enforce_extra: "remove"
  });

  return ActiveUser;
};


module.exports = ActiveUserWrapper;
