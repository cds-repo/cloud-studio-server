"use strict";

const ConfigurationWrapper = (thinky) => {
  const type = thinky.type;

  const Configuration = thinky.createModel("Configurations", {
    id: type.string(),
    key: type.string().required(),
    value: type.string(),
    createdAt: type.date(),
    updatedAt: type.date()
  }, {
    enforce_extra: "remove"
  });

  Configuration.ensureIndex("key");

  return Configuration;
};


module.exports = ConfigurationWrapper;
