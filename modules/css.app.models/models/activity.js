"use strict";

const ActivityWrapper = (thinky) => {
  const type = thinky.type;

  const Activity = thinky.createModel("Activities", {
    id: type.string(),
    userID: type.string(),
    projectID: type.string(),
    activity: type.string(),
    createdAt: type.date(),
    updatedAt: type.date()
  }, {
    enforce_extra: "remove"
  });

  return Activity;
};


module.exports = ActivityWrapper;
