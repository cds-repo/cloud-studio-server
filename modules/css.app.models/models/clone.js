"use strict";

const CloneWrapper = (thinky) => {
  const type = thinky.type;

  const Clone = thinky.createModel("Clones", {
    id: type.string(),
    userID: type.string(),
    projectID: type.string(),
    dockerPort: type.number().integer().min(0),
    createdAt: type.date(),
    updatedAt: type.date()
  }, {
    enforce_extra: "remove"
  });

  return Clone;
};


module.exports = CloneWrapper;
