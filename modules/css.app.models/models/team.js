"use strict";

const TeamWrapper = (thinky) => {
  const type = thinky.type;

  const Team = thinky.createModel("Teams", {
    id: type.string(),
    name: type.string().min(4).required(),
    leadID: type.string(),
    createdAt: type.date(),
    updatedAt: type.date()
  }, {
    enforce_extra: "remove"
  });

  return Team;
};


module.exports = TeamWrapper;
