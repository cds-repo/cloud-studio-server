"use strict";

const validator = require('validator');

const UserWrapper = (thinky) => {
  const type = thinky.type;

  const User = thinky.createModel("Users", {
    id: type.string(),
    username: type.string().min(4).required(),
    password: type.string().required(),
    email: type.string().required().validator(validator.isEmail),
    firstName: type.string().required(),
    lastName: type.string().required(),
    rank: type.number().integer().default(1),
    createdAt: type.date(),
    updatedAt: type.date()
  }, {
    enforce_extra: "remove"
  });

  User.define('isAdmin', function isAdmin() {
    return this.rank === 9;
  });
  User.define('isManager', function isManager() {
    return this.rank === 5;
  });
  User.define('isLead', function isLead() {
    return this.rank === 4;
  });
  User.define('isDeveloper', function isDeveloper() {
    return this.rank === 1;
  });
  User.define('isAtLeast', function isAtLeast(ranking) {
    ranking = ranking.toUpperCase();
    switch (ranking) {
      case 'ADMIN':
        return this.rank === 9;

      case 'MANAGER':
        return this.rank >= 5;

      case 'LEAD':
        return this.rank >= 4;

      case 'DEVELOPER':
      default:
        return this.rank >= 1;
    }
  });

  return User;
};


module.exports = UserWrapper;
