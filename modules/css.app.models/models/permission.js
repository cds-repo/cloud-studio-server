"use strict";

const PermissionWrapper = (thinky) => {
  const type = thinky.type;

  const Permission = thinky.createModel("Permissions", {
    id: type.string(),
    userID: type.string(),
    permission: type.string().required(),
    createdAt: type.date(),
    updatedAt: type.date()
  }, {
    enforce_extra: "remove"
  });

  return Permission;
};


module.exports = PermissionWrapper;
