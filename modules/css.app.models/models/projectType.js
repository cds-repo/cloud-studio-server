"use strict";

const ProjectTypeWrapper = (thinky) => {
  const type = thinky.type;

  const ProjectType = thinky.createModel("ProjectTypes", {
    id: type.string(),
    name: type.string().required(),
    logo: type.string().required(),
    description: type.string(),
    docker_file: type.string(),
    multiple_instances: type.boolean().required(),
    createdAt: type.date(),
    updatedAt: type.date()
  }, {
    enforce_extra: "remove"
  });

  return ProjectType;
};


module.exports = ProjectTypeWrapper;
