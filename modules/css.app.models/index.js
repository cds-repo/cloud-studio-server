"use strict";
const _ = require("lodash");

const AppModels = function AppModels(engine) {
  const thinky = require('thinky')(engine.config.db);
  const self = {};

  engine.models = require('./models')(thinky);
  require("./links")(engine.models);
  engine.models.UserRank = require('./userRanks');
  engine.app.r = thinky.r;

  return _.extend(self, this);
};

module.exports = AppModels;
