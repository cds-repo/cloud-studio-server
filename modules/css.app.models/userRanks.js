const Ranks = {
  ADMIN: 9,
  MANAGER: 5,
  LEAD: 4,
  DEV: 1
};

module.exports = {
  get ADMIN() {
    return Ranks.ADMIN;
  },
  get MANAGER() {
    return Ranks.MANAGER;
  },
  get LEAD() {
    return Ranks.LEAD;
  },
  get DEV() {
    return Ranks.DEV;
  },

  toString: (id) => {
    switch (id) {
      case Ranks.ADMIN:
        return "Administrator";
      case Ranks.MANAGER:
        return "Manager";
      case Ranks.LEAD:
        return "Lead";
      case Ranks.DEV:
      default:
        return "Developer";
    }
  }
};
