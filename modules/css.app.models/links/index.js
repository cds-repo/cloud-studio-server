"use strict";
const _ = require('lodash');

module.exports = function Links(model) {
  const AuditFields = function AuditFields(next) {
    const obj = this;
    if (!obj.createdAt) {
      obj.createdAt = new Date();
    }
    obj.updatedAt = new Date();

    next();
  };

  _.forOwn(model, (value, key) => {
    value.pre("save", AuditFields);
  });

  require("./user")(model);
  require("./project")(model);
  require("./member")(model);
};
