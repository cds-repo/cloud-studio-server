"use strict";

module.exports = function member(models) {
  const Team = models.Team;

  Team.pre('save', function TeamSaveMembers(next) {
    const self = this;
    if (self.members === null || self.members === undefined) {
      self.members = [];
      self.members.push(self.leadID);
    }
    next();
  });
};
