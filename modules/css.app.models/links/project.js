"use strict";

module.exports = function ProjectLinks(models) {
  const Activity = models.Activity;
  const Clone = models.Clone;
  const Project = models.Project;
  const Team = models.Team;
  const ProjectType = models.ProjectType;

  /* -- Activity <-> Project -- */
  Activity.belongsTo(Project, "project", "projectID", "id");
  Project.hasMany(Activity, "activities", "id", "projectID");
  /* -- Project <-> Activity -- */

  /* -- Clone <-> Project -- */
  Clone.belongsTo(Project, "project", "projectID", "id");
  Project.hasMany(Clone, "clones", "id", "projectID");
  /* -- Project <-> Clone -- */

  /* -- Project <-> Project Type -- */
  Project.belongsTo(ProjectType, "type", "typeID", "id");
  ProjectType.hasMany(Project, "projects", "id", "typeID");
  /* -- Project Type <-> Project -- */

  /* -- Project <-> Team -- */
  Project.belongsTo(Team, "team", "teamID", "id");
  Team.hasMany(Project, "projects", "id", "teamID");
  /* -- Team <-> Project -- */

  Project.pre("save", function ProjectEnrichment(next) {
    if (this.activities === null || this.activities === undefined) {
      this.activities = [];
      this.activities.push({
        userID: this.ownerID,
        activity: "PROJECT|NEW"
      });
    } else {
      this.activities.push({
        userID: this.ownerID,
        activity: "PROJECT|UPDATE"
      });
    }

    if (this.clones === null || this.clones === undefined) {
      this.clones = [];
      this.clones.push({
        userID: this.ownerID
      });
    }

    next();
  });

  Clone.pre("save", function CloneEnrichment(next) {
    let statusString = "";

    if (this.id) {
      // statusString = `CLONE|UPD|${this.dockerPort || 0}`;
      next();
      return;
    } else {
      statusString = "CLONE|NEW";
    }

    const activity = new Activity({
      projectID: this.projectID,
      userID: this.userID,
      activity: statusString
    });

    activity.save()
      .then(() => {
        next();
      })
      .catch((e) => {
        next(e);
      });
  });

  Clone.pre("delete", function CloneDeletion(next) {
    const activity = new Activity({
      projectID: this.projectID,
      userID: this.userID,
      activity: "CLONE|DLT"
    });

    activity.save()
      .then(() => {
        next();
      })
      .catch((e) => {
        next(e);
      });
  });
};
