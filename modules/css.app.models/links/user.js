"use strict";

module.exports = function UserLinks(models) {
  const ActiveUser = models.ActiveUser;
  const Activity = models.Activity;
  const Clone = models.Clone;
  const Permission = models.Permission;
  const Project = models.Project;
  const Team = models.Team;
  const User = models.User;

  /* -- Active User <-> User -- */
  ActiveUser.belongsTo(User, "user", "userID", "id");
  User.hasOne(ActiveUser, "activeInstance", "id", "userID");
  /* -- User <-> Active User -- */

  /* -- Activity <-> User -- */
  Activity.belongsTo(User, "user", "userID", "id");
  User.hasMany(Activity, "activities", "id", "userID");
  /* -- User <-> Activity -- */

  /* -- Clone <-> User -- */
  Clone.belongsTo(User, "user", "userID", "id");
  User.hasMany(Clone, "clones", "id", "userID");
  /* -- User <-> Clone -- */

  /* -- Project <-> User -- */
  Project.belongsTo(User, "owner", "ownerID", "id");
  User.hasMany(Project, "ownProjects", "id", "ownerID");
  /* -- User <-> Project -- */

  /* -- Team <-> User -- */
  Team.belongsTo(User, "lead", "leadID", "id");
  User.hasMany(Team, "leads", "id", "leadID");

  Team.hasAndBelongsToMany(User, "members", "id", "id");
  User.hasAndBelongsToMany(Team, "teams", "id", "id");
  /* -- User <-> Team -- */

  /* -- Permission <-> User -- */
  Permission.belongsTo(User, "user", "userID", "id");
  User.hasMany(Permission, "permissions", "id", "userID");
  /* -- User <-> Permission -- */
};
