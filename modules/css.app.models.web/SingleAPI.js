"use strict";
const API = require("../../engine/core/API");
const _ = require("lodash");

class SingleAPI extends API {
  constructor(engine, path) {
    super(engine, path);
    this._permissionBase = "css.api.model";
    this._repository = null;
    this._own = false;
    this._cascadeDelete = null;

    this._permissionSuppression = {
      own: false,
      all: false,
      one: false,
      create: false,
      update: false,
      delete: false
    };

    this._registerAPI = {
      own: false,
      all: true,
      one: true,
      create: true,
      update: true,
      delete: true
    };
  }

  register() {
    const self = this;

    if (self._own && self._registerAPI.own) {
      this.router.get('/own', (req, res) => this.getOwnModels(self, req, res));
    }
    if (self._registerAPI.all) {
      this.router.get('/', (req, res) => this.getModels(self, req, res));
    }
    if (self._registerAPI.one) {
      this.router.get('/:id', (req, res) => this.getModel(self, req, res));
    }
    if (self._registerAPI.create) {
      this.router.post('/', (req, res) => this.postModel(self, req, res));
    }
    if (self._registerAPI.update) {
      this.router.put('/:id', (req, res) => this.putModel(self, req, res));
    }
    if (self._registerAPI.delete) {
      this.router.delete('/:id', (req, res) => this.deleteModel(self, req, res));
    }
  }

  get repository() {
    return this._repository;
  }

  set repository(repo) {
    this._repository = repo;
  }

  get permissionBase() {
    return this._permissionBase;
  }

  set permissionBase(base) {
    this._permissionBase = base;
  }

  get surpressPermission() {
    return this._permissionSuppression;
  }

  set surpressPermission(section) {
    const sections = section.split(",");
    const self = this;

    _.forOwn(sections, (v) => {
      self._permissionSuppression[v] = true;
    });
  }

  get own() {
    return this._own;
  }

  set own(own) {
    this._own = own;
    this._registerAPI.own = own;
  }

  get cascadeDelete() {
    return this._cascadeDelete;
  }

  set cascadeDelete(cascadeDelete) {
    this._cascadeDelete = cascadeDelete;
  }

  set registerAPI(section) {
    const sections = section.split(",");
    const self = this;

    _.forOwn(sections, (v) => {
      self._registerAPI[v.trim()] = true;
    });
  }

  set registerOnlyAPI(section) {
    const sections = section.split(",");
    const self = this;

    _.forOwn(self._registerAPI, (v, k) => {
      self._registerAPI[k.trim()] = false;
    });

    _.forOwn(sections, (v) => {
      self._registerAPI[v.trim()] = true;
    });
  }

  set unregisterAPI(section) {
    const sections = section.split(",");
    const self = this;

    _.forOwn(sections, (v) => {
      self._registerAPI[v.trim()] = false;
    });
  }

  enrichMultiple(models) {

  }

  enrich(model) {

  }

  getOwnModels(self, req, res) {
    if (!self._permissionSuppression.own) {
      self.allowed(req.user, 'getOwnModels', `${self._permissionBase}.own`);
    }

    let joinedElements = {};
    try {
      joinedElements = JSON.parse(req.get('X-CDS-JoinedElements'));
    } catch (e) {
      joinedElements = self.cascadeDelete || {};
    }

    self.repository
      .filterJoin({
        userID: req.user.id
      }, joinedElements)
      .then((models) => {
        self.enrichMultiple(models);
        res.json(models);
      })
      .catch((e) => {
        res.throw(e);
      });
  }

  getModels(self, req, res) {
    if (!self._permissionSuppression.all) {
      self.allowed(req.user, 'getModels', `${self._permissionBase}.all`);
    }

    let joinedElements = {};
    try {
      joinedElements = JSON.parse(req.get('X-CDS-JoinedElements'));
    } catch (e) {
      joinedElements = self.cascadeDelete || {};
    }

    self.repository
      .getAllJoin(joinedElements)
      .then((models) => {
        // self.enrichMultiple(models);
        res.json(models);
      })
      .catch((e) => {
        res.throw(e);
      });
  }

  getModel(self, req, res) {
    if (!self._permissionSuppression.one) {
      self.allowed(req.user, 'getModel', `${self._permissionBase}.one`);
    }

    let joinedElements = {};
    try {
      joinedElements = JSON.parse(req.get('X-CDS-JoinedElements'));
    } catch (e) {
      joinedElements = self.cascadeDelete || {};
    }

    self.repository
      .getJoin(req.params.id, joinedElements)
      .then((model) => {
        // self.enrich(model);
        res.json(model);
      })
      .catch((e) => {
        res.throw(e);
      });
  }

  postModel(self, req, res) {
    if (!self._permissionSuppression.create) {
      self.allowed(req.user, 'postModel', `${self._permissionBase}.create`);
    }

    let joinedElements = {};
    try {
      joinedElements = JSON.parse(req.get('X-CDS-JoinedElements'));
    } catch (e) {
      joinedElements = self.cascadeDelete || {};
    }

    self.repository
      .createJoin(req.body, joinedElements)
      .then((model) => {
        self.enrich(model);

        res.json(model);
      })
      .catch((e) => {
        res.throw(e);
      });
  }

  putModel(self, req, res) {
    if (!self._permissionSuppression.update) {
      self.allowed(req.user, 'putModel', `${self._permissionBase}.update`);
    }

    self.repository
      .update(
        req.params.id,
        req.body
      )
      .then((model) => {
        self.enrich(model);
        res.json(model);
      })
      .catch((e) => {
        res.throw(e);
      });
  }

  deleteModel(self, req, res) {
    if (!self._permissionSuppression.delete) {
      self.allowed(req.user, 'deleteModel', `${self._permissionBase}.delete`);
    }

    if (self.path === "user" && req.params.id === req.user.id) {
      req.logout();
    }

    const deleteCall = self.cascadeDelete ?
      self.repository.removeJoin(req.params.id, self.cascadeDelete) :
      self.repository.remove(req.params.id);

    deleteCall
      .then(() => {
        res.ok("model deleted!");
      })
      .catch((e) => {
        res.throw(e);
      });
  }
}

module.exports = SingleAPI;
