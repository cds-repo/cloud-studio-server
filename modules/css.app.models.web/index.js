"use strict";
const _ = require("lodash");

const ModelsWeb = function ModelsWeb(engine) {
  const self = {};

  engine.permissions.import(require('./permissions.json'));
  engine.routes.build(__dirname, 'routes');

  return _.extend(self, this);
};


module.exports = ModelsWeb;
