"use strict";
const _ = require('lodash');
const SingleAPI = require("../SingleAPI");


class UserAPI extends SingleAPI {
  constructor(engine) {
    super(engine, 'user');
    this.repository = engine.modules.get("css.app.models.api").UserRepository;
    this.configRepository = engine.modules.get("css.app.models.api").ConfigurationRepository;
    this.permissionBase = "css.api.users";
    this.surpressPermission = 'create';
  }

  register() {
    this.router.get('/:id/no_permission', (req, res) => this.getNoPermission(this, req, res));

    super.register();
  }

  postModel(self, req, res) {
    let userRankDefaults = "cds.permissions.default.user";

    if (req.body.rank) {
      switch (req.body.rank) {
        case 4:
          userRankDefaults = "cds.permissions.default.lead";
          break;
        case 5:
          userRankDefaults = "cds.permissions.default.manager";
          break;
        case 9:
          userRankDefaults = "cds.permissions.default.admin";
          break;
        default:
          req.body.rank = 1;
      }
    }

    self.configRepository
      .filter({
        key: userRankDefaults
      })
      .then((configs) => {
        const user = req.body;
        let config = configs.pop();
        user.permissions = [];
        if (_.isObject(config) && _.isString(config.value) && config.value.length !== 0) {
          config = JSON.parse(config.value);
          _.forEach(config, (value) => {
            user.permissions.push({
              permission: value
            });
          });
        }

        super.postModel(self, req, res);
      })
      .catch((error) => {
        self.engine.log.error(error.stack);
        res.throw(error);
      });
  }

  enrich(user) {
    delete user.password;
  }

  enrichMultiple(users) {
    _.forOwn(users, (v) => {
      delete v.password;
    });
  }


  deleteModel(self, req, res) {
    if (!self.surpressPermission.delete) {
      self.allowed(req.user, 'deleteModel', `${self._permissionBase}.delete`);
    }

    if (self.path === "user" && req.params.id === req.user.id) {
      req.logout();
    }

    const date = (new Date()).toISOString();

    self.repository
      .update(req.params.id, {
        password: date,
        confirmPassword: date
      })
      .then(() => {
        res.ok("model deleted!");
      })
      .catch((e) => {
        res.throw(e);
      });
  }

  getNoPermission(self, req, res) {
    self.allowed(req.user, 'getNoPermission', `${self._permissionBase}.userPermission`);
    const permissions = self.engine.permissions.all();

    self.repository
      .getJoin(req.params.id, {
        permissions: true
      })
      .then((userX) => {
        let permissionList = [];

        _.each(permissions, (v, k) => {
          const userPermission = _.filter(userX.permissions, (permission) => permission.permission === k);

          if (userPermission.length === 0) {
            permissionList.push(k);
          }
        });

        res.json(permissionList);
      })
      .catch((e) => {
        res.throw(e);
      });
  }
}

module.exports = UserAPI;
