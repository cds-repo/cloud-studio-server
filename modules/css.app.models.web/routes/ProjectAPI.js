"use strict";
const SingleAPI = require("../SingleAPI");
const Promise = require('bluebird');
const _ = require('lodash');

class ProjectAPI extends SingleAPI {
  constructor(engine) {
    super(engine, 'project');
    this.repository = engine.modules.get("css.app.models.api").ProjectRepository;
    this.permissionBase = 'css.api.project';
    this.own = true;
    this.cascadeDelete = {
      activities: true,
      clones: true
    };
  }

  postModel(self, req, res) {
    if (!req.body.ownerID && req.user) {
      req.body.ownerID = req.user.id;
    }

    if (req.body.restricted) {
      req.body.restricted = req.body.restricted === "1";
    }

    super.postModel(self, req, res);
  }

  putModel(self, req, res) {
    if (!self._permissionSuppression.update) {
      self.allowed(req.user, 'putModel', `${self._permissionBase}.update`);
    }

    if (req.body.restricted) {
      req.body.restricted = req.body.restricted === "1";
    }

    if (req.body.teamID) {
      if (req.body.teamID === "0") {
        req.body.teamID = null;
      }
      self._teamMembers(self, req, res);
      return;
    }

    self.repository
      .update(
        req.params.id,
        req.body
      )
      .then((model) => {
        self.enrich(model);
        res.json(model);
      })
      .catch((e) => {
        res.throw(e);
      });
  }

  _teamMembers(self, req, res) {
    const Team = self.engine.models.Team;
    const Project = self.engine.models.Project;
    let _project;

    Project
      .get(req.params.id)
      .getJoin({
        team: {
          members: true
        },
        activities: true,
        clones: true
      })
      .run()
      .then((project) => {
        _project = project;

        return Team
          .get(req.body.teamID)
          .getJoin({
            members: true
          })
          .run()
          .then((team) => team)
          .catch((e) => {
          });
      })
      .then((team) => self._manipulateMembers(self, _project, team))
      .then(() => {
        _project = _.extend(_project, req.body);

        return self.repository.validate(_project, req.body, "update")
          .then((modelValidated) => self.repository.enrich(modelValidated, req.body, "update"))
          .then((modelEnriched) => modelEnriched.saveAll({
            activities: true
          }));
      })
      .then((result) => {
        res.json(result);
      })
      .catch((e) => {
        console.log(e.stack);
        res.throw(e);
      });
  }

  _manipulateMembers(self, project, team) {
    const toDelete = [];
    const toAdd = [];
    const Clone = self.engine.models.Clone;

    if (project.team) {
      _.each(project.team.members, (member) => {
        if (member.id === project.ownerID) {
          return;
        }
        if (team && _.filter(team.members, (newMember) => newMember.id === member.id).length !== 0) {
          return;
        }

        toDelete.push(member.id);
      });
    }

    if (team) {
      _.each(team.members, (member) => {
        if (member.id === project.ownerID) {
          return;
        }
        if (project.team && _.filter(project.team.members, (oldMember) => oldMember.id === member.id).length !== 0) {
          return;
        }
        if (_.filter(project.clones, (clone) => clone.userID === member.id).length !== 0) {
          return;
        }

        toAdd.push(new Clone({
          projectID: project.id,
          userID: member.id
        }));
      });
    }

    return Clone
      .filter({
        projectID: project.id
      })
      .run()
      .then((results) => {
        results = _.filter(results, (clone) => toDelete.indexOf(clone.userID) >= 0);

        return Promise
          .each(results, (result) => result.delete())
          .then(() => Promise.each(toAdd, (clone) => clone.save()));
      });
  }

}

module.exports = ProjectAPI;
