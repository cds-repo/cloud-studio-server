"use strict";
const SingleAPI = require("../SingleAPI");

class ProjectTypeAPI extends SingleAPI {
  constructor(engine) {
    super(engine, 'projectType');
    this.repository = engine.modules.get("css.app.models.api").ProjectTypeRepository;
    this.permissionBase = 'css.api.projectType';
    this.cascadeDelete = {
      projects: {
        clones: true,
        activities: true
      }
    };
  }

  postModel(self, req, res) {
    if (req.body.multiple_instances) {
      req.body.multiple_instances = req.body.multiple_instances === "1";
    }

    super.postModel(self, req, res);
  }

  putModel(self, req, res) {
    if (req.body.multiple_instances) {
      req.body.multiple_instances = req.body.multiple_instances === "1";
    }

    super.putModel(self, req, res);
  }
}

module.exports = ProjectTypeAPI;
