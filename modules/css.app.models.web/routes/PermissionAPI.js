"use strict";
const SingleAPI = require("../SingleAPI");
const str2json = require("string-to-json");
const Promise = require('bluebird');
const _ = require('lodash');

class PermissionAPI extends SingleAPI {
  constructor(engine) {
    super(engine, 'permission');
    this.repository = engine.modules.get("css.app.models.api").PermissionRepository;
    this.permissionBase = 'css.api.permission';
    this.unregisterAPI = 'update';
    this.own = true;
  }

  register() {
    this.router.get('/fullList', (req, res) => {
      this.allowed(req.user, 'fullPermissionList', `${this.permissionBase}.fullList`);
      if(req.query.noConvert) {
        res.json(this.engine.permissions.all());
        return;
      }
      res.json(str2json.convert(this.engine.permissions.all()));
    });

    this.router.put('/default', (req, res) => this.putLoadDefault(this, req, res));
    this.router.delete('/all', (req, res) => this.deleteAll(this, req, res));

    super.register();
  }

  putLoadDefault(self, req, res) {
    self.allowed(req.user, 'putLoadDefault', `${self.permissionBase}.loadDefault`);

    const Config = self.engine.modules.get('css.app.models.api').ConfigurationRepository;
    const User = self.engine.modules.get('css.app.models.api').UserRepository;
    const Permission = self.engine.models.Permission;
    let defaultsTo = "user";
    let permissions = [];

    switch (req.body.type.toUpperCase()) {
      case "MANAGER":
        defaultsTo = "manager";
        break;
      case "LEAD":
        defaultsTo = "lead";
        break;
      case "ADMINISTRATOR":
        defaultsTo = "admin";
        break;
      default:
      case "DEVELOPER":
        defaultsTo = "user";
        break;
    }

    Config
      .filter({key: `cds.permissions.default.${defaultsTo}`})
      .then((result) => {
        if (result && result.length > 0) {
          permissions = JSON.parse(result.pop().value);
        }
        if (defaultsTo === "admin") {
          permissions = Object.keys(self.engine.permissions.all());
        }

        return User
          .getJoin(req.body.userID, {
            permissions: true
          });
      })
      .then((user) => {

        _.each(permissions, (v) => {
          const userPermission = _.filter(user.permissions, (permission) => {
            return permission.permission === v;
          });
          if (userPermission.length === 0) {
            user.permissions.push({
              permission: v
            });
          }
        });

        return user
          .saveAll({
            permissions: true
          })
          .then(() => res.ok("Permissions added"));
      })
      .catch((err) => {
        console.log(err);
      });
  }

  deleteAll(self, req, res) {
    self.allowed(req.user, 'deleteAll', `${self.permissionBase}.deleteAll`);

    const Permission = self.engine.models.Permission;

    Permission
      .filter({
        userID: req.body.userID
      })
      .run()
      .then((permissions) => Promise.each(permissions, (result) => result.delete()))
      .then(() => res.ok("Permissions deleted!"))
      .catch((err) => {
        console.log(err);
      });
  }
}

module.exports = PermissionAPI;
