"use strict";
const SingleAPI = require("../SingleAPI");

class CloneAPI extends SingleAPI {
  constructor(engine) {
    super(engine, 'clone');
    this.repository = engine.modules.get("css.app.models.api").CloneRepository;
    this.permissionBase = 'css.api.clone';
    this.own = true;
  }

  register() {
    this.router.post('/try', (req, res) => this.postTryClone(this, req, res));

    super.register();
  }

  postTryClone(self, req, res) {
    self.allowed(req.user, 'postTryClone', `${self.permissionBase}.tryClone`);

    self.repository
      .filter({
        userID: req.body.userID,
        projectID: req.body.projectID
      })
      .then((result) => {
        if (result.length === 0) {
          return self.repository.create({
            userID: req.body.userID,
            projectID: req.body.projectID
          });
        }
        return result;
      })
      .then((result) => {
        res.ok("Clone created");
      })
      .catch((e) => {
        res.throw(e);
      });
  }
}

module.exports = CloneAPI;
