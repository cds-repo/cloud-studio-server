"use strict";
const SingleAPI = require("../SingleAPI");

class ConfigurationAPI extends SingleAPI {
  constructor(engine) {
    super(engine, 'configuration');
    this.repository = engine.modules.get("css.app.models.api").ConfigurationRepository;
    this.permissionBase = 'css.api.configuration';
    this.surpressPermission = 'one';
  }

  register() {
    super.register();

    this.router.get('/key/:key', (req, res) => this.getKey(this, req, res));
  }

  getKey(self, req, res) {
    self.repository
      .filter({
        key: req.params.key
      })
      .then((models) => {
        if (models.length === 0) {
          res.json({});
          return;
        }
        res.json(models.pop());
      })
      .catch((e) => {
        res.throw(e);
      });
  }
}

module.exports = ConfigurationAPI;
