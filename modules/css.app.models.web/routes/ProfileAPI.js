"use strict";
const API = require("../../../engine/core/API");
const _ = require("lodash");

class ProfileAPI extends API {
  constructor(engine) {
    super(engine, "profile");
    this.repository = engine.modules.get("css.app.models.api").UserRepository;
  }

  register() {
    this.router.get('/', (req, res) => this.viewProfile(this, req, res));
    this.router.put('/', (req, res) => this.updateProfile(this, req, res));
    this.router.delete('/', (req, res) => this.deleteProfile(this, req, res));
  }

  viewProfile(self, req, res) {
    self.allowed(req.user, 'viewProfile', 'css.api.profile.view');

    const user = _.cloneDeep(req.user);
    delete user.password;

    res.json(user);
  }

  updateProfile(self, req, res) {
    self.allowed(req.user, 'updateProfile', 'css.api.profile.update');

    _.forEach(req.body, (v, k) => {
      switch (k) {
        case "username":
        case "rank":
        case "id":
          delete req.body[k];
          break;
      }
    });

    if (_.isEmpty(req.body.password)) {
      delete req.body.password;
    }

    self.repository
      .update(
        req.user.id,
        req.body
      )
      .then((model) => {
        res.json(model);
      })
      .catch((e) => {
        res.throw(e);
      });
  }

  deleteProfile(self, req, res) {
    self.allowed(req.user, 'deleteProfile', 'css.api.profile.delete');
    // TODO: User delete + logout
    res.error("NotAllowed", "To delete your account ask and administrator", 403);
  }
}

module.exports = ProfileAPI;
