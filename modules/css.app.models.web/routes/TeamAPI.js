"use strict";
const SingleAPI = require("../SingleAPI");
const _ = require("lodash");

class TeamAPI extends SingleAPI {
  constructor(engine) {
    super(engine, 'team');
    this.repository = engine.modules.get("css.app.models.api").TeamRepository;
    this.userRepository = engine.modules.get("css.app.models.api").UserRepository;
    this.Activity = engine.modules.get("css.app.models").Activity;
    this.permissionBase = 'css.api.team';
    this.own = true;
    this.cascadeDelete = {
      members: true
    };
  }

  postModel(self, req, res) {
    if (!req.body.leadID && req.user) {
      req.body.leadID = req.user.id;
    }

    super.postModel(self, req, res);
  }

  deleteModel(self, req, res) {
    self.repository
      .getJoin(req.params.id, {
        members: true
      })
      .then((team) => {
        team.members = [];
        return team.saveAll();
      })
      .then(() => {
        super.deleteModel(self, req, res);
      })
      .catch((e) => {
        res.throw(e);
      });
  }

  register() {
    super.register();

    this.router.get('/:team/members', (req, res) => this.getMembers(this, req, res));
    this.router.post('/:team/members', (req, res) => this.postMember(this, req, res));
    this.router.delete('/:team/members/:id', (req, res) => this.deleteMember(this, req, res));
  }

  getMembers(self, req, res) {
    self.allowed(req.user, 'getMembers', `${self.permissionBase}.members.all`);

    self.repository
      .getJoin(req.params.team, {
        members: {
          user: true
        }
      })
      .then((team) => {
        res.json(team.members);
      })
      .catch((e) => {
        res.throw(e);
      });
  }

  postMember(self, req, res) {
    self.allowed(req.user, 'postMember', `${self.permissionBase}.members.attach`);
    let _team;

    self.repository
      .getJoin(req.params.team, {
        members: true,
        projects: true
      })
      .then((team) => {
        _team = team;

        return self.userRepository
          .getJoin(req.body.userID);
      })
      .then((user) => {
        if (!_.isObject(user)) {
          throw new Error("Required user doesn't exists");
        }

        _team.members.push(user);

        const activities = [];
        for (let i = 0; i < _team.projects.length; i++) {
          activities.push({
            projectID: _team.projects[i].id,
            userID: user.id,
            activity: `MEMBER|NEW|${user.id}`
          });
        }

        if (activities.length > 0) {
          return self.Activity(activities)
            .save()
            .then((actvs) => _team.saveAll({
              members: true
            }));
        }

        return _team.saveAll({
          members: true
        });
      })
      .then((team) => {
        res.json(team);
      })
      .catch((e) => {
        res.throw(e);
      });
  }

  deleteMember(self, req, res) {
    self.allowed(req.user, 'deleteMember', `${self.permissionBase}.members.detach`);

    self.repository
      .getJoin(req.params.team, {
        members: true,
        projects: true
      })
      .then((team) => {
        for (let i = 0; i < team.members.length; i++) {
          if (team.members[i].id === req.params.id) {
            delete team.members[i];
          }
        }

        const activities = [];
        for (let i = 0; i < team.projects.length; i++) {
          activities.push({
            projectID: team.projects[i].id,
            userID: req.params.id,
            activity: `MEMBER|DLT`
          });
        }

        if (activities.length > 0) {
          return self.Activity(activities)
            .save()
            .then((actvs) => team.saveAll({
              members: true
            }));
        }

        return team.saveAll({
          members: true
        });
      })
      .then((team) => {
        res.json(team);
      })
      .catch((e) => {
        res.throw(e);
      });
  }
}

module.exports = TeamAPI;
