"use strict";
const API = require("../../../engine/core/API");
const express = require("express");
const child_process = require("child_process");
const spawn = child_process.spawn;
const path = require('path');
// const _ = require("lodash");

class GitProjectAPI extends API {
  constructor(engine) {
    super(engine, 'git/project');
    this.gitFSPath = path.resolve(`${engine.config.fs.path}/gitfs`);
  }

  register() {
    this.router.get('/:id/info/refs', (req, res) => this.getInfoRefs(this, req, res));
    this.router.post('/:id/git-receive-pack', (req, res) => this.postReceivePack(this, req, res));
    // this.router.get('/own', (req, res) => this.getOwnModels(this, req, res));
  }

  getInfoRefs(self, req, res) {
    res.setHeader('Expires', 'Fri, 01 Jan 1980 00:00:00 GMT');
    res.setHeader('Pragma', 'no-cache');
    res.setHeader('Cache-Control', 'no-cache, max-age=0, must-revalidate');
    res.setHeader('Content-Type', 'application/x-git-receive-pack-advertisement');

    const packet = "# service=git-receive-pack\n";
    const length = packet.length + 4;
    const hex = "0123456789abcdef";
    let prefix = hex.charAt((length >> 12) & 0xf);
    prefix = prefix + hex.charAt((length >> 8) & 0xf);
    prefix = prefix + hex.charAt((length >> 4) & 0xf);
    prefix = prefix + hex.charAt(length & 0xf);
    res.write(`${prefix}${packet}0000`);

    const git = spawn('git-receive-pack', ['--stateless-rpc', '--advertise-refs', `${self.gitFSPath}/${req.params.id}`]);
    git.stdout.pipe(res);
    git.stderr.on('data', (data) =>
      console.log(`stderr: ${data}`));
    git.on('exit', () => res.end());
  }

  postReceivePack(self, req, res) {
    res.setHeader('Expires', 'Fri, 01 Jan 1980 00:00:00 GMT');
    res.setHeader('Pragma', 'no-cache');
    res.setHeader('Cache-Control', 'no-cache, max-age=0, must-revalidate');
    res.setHeader('Content-Type', 'application/x-git-receive-pack-result');

    const packet = "# service=git-receive-pack\n";
    const length = packet.length + 4;
    const hex = "0123456789abcdef";
    let prefix = hex.charAt((length >> 12) & 0xf);
    prefix = prefix + hex.charAt((length >> 8) & 0xf);
    prefix = prefix + hex.charAt((length >> 4) & 0xf);
    prefix = prefix + hex.charAt(length & 0xf);
    res.write(`${prefix}${packet}0000`);

    const git = spawn('git-receive-pack', ['--stateless-rpc', `${self.gitFSPath}/${req.params.id}`]);

    req.pipe(git.stdin);
    git.stdout.pipe(res);
    git.stderr.on('data', (data) =>
      console.log(`stderr: ${data}`));
    git.on('exit', () => {
      console.log(arguments);
      res.end();});
  }

}

module.exports = GitProjectAPI;
