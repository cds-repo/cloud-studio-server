"use strict";
const _ = require("lodash");
const bodyParser = require('body-parser');

const GitModule = function GitModule(engine) {
  const self = {};

  engine.permissions.import(require('./permissions.json'));
  engine.routes.build(__dirname, 'routes');
 //Test Comment
  engine.app.use(bodyParser.raw({
    limit: engine.config.fs.rawBodySize
  }));

  return _.extend(self, this);
};


module.exports = GitModule;
