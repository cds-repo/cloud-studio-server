"use strict";

class NoModeException extends Error {
  constructor() {
    super("You haven't specified any mode!");
  }
}

module.exports = NoModeException;
