"use strict";

class NoSourceException extends Error {
  constructor() {
    super("You haven't specified any source file or folder!");
  }
}

module.exports = NoSourceException;
