"use strict";

class NoDestinationException extends Error {
  constructor() {
    super("You haven't specified any destination file or folder!");
  }
}

module.exports = NoDestinationException;
