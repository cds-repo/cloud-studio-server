"use strict";

class FileNotExistsException extends Error {
  constructor(file) {
    super(`The specified file or folder [${file}] doesn't exists!`);
  }
}

module.exports = FileNotExistsException;
