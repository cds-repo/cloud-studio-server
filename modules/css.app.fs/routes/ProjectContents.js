"use strict";
const API = require("../../../engine/core/API");
const FileHandling = require("../fs/FileHandling");
const FolderHandling = require("../fs/FolderHandling");
const FileType = require("../fs/FileType");
const _ = require("lodash");

class ProjectContents extends API {
  constructor(engine) {
    super(engine, "project");
    this.fs = {
      FileHandling: new FileHandling(engine),
      FolderHandling: new FolderHandling(engine)
    };
    this.ProjectRepository = engine.modules.get('css.app.models.api').ProjectRepository;
  }

  register() {
    this.router.get('/:id/tree', (req, res) => this.getFileTree(this, req, res));
    this.router.get('/:id/listing', (req, res) => this.getListing(this, req, res));
    this.router.get('/:id/read', (req, res) => this.getFile(this, req, res));
    this.router.put('/:id/save', (req, res) => this.putSaveFile(this, req, res));
    this.router.post('/:id/create', (req, res) => this.postCreateFile(this, req, res));
    this.router.delete('/:id/delete', (req, res) => this.deleteFile(this, req, res));
    this.router.put('/:id/move', (req, res) => this.putMove(this, req, res));
  }

  _check(id, path, callback) {
    const join = {
      owner: true
    };

    return this
      .ProjectRepository
      .getJoin(id, join)
      .then((model) => {
        if (!model) {
          throw new Error("Model not found");
        }

        const projectPath = `projects/${model.id}/${path}`;
        const basePath = `projects/${model.id}`;
        return callback(model, projectPath, basePath);
      });
  }

  getFileTree(self, req, res) {
    self.allowed(req.user, 'getFileTree', 'css.fs.project.getFileTree');

    self
      ._check(req.params.id, req.query.path, (model, projectPath) => {
        res.json(self.fs.FolderHandling.readTree(projectPath));
      })
      .catch((e) => {
        res.throw(e);
      });
  }

  getListing(self, req, res) {
    self.allowed(req.user, 'getListing', 'css.fs.project.getListing');

    self
      ._check(req.params.id, req.query.path, (model, projectPath) => {
        res.json(self.fs.FolderHandling.read(projectPath));
      })
      .catch((e) => {
        res.throw(e);
      });
  }

  _readFile(result, path, base) {
    if (typeof(result) === "object") {
      result = result.toString("base64");
    }

    const fileInfo = this.fs.FileHandling.pathinfo(path);
    let lang = fileInfo.extname || ".txt";
    if (lang.length !== 0) {
      lang = lang.substring(1);
    }

    let pathDetails = path.substring(base.length);
    pathDetails = pathDetails.substring(0, pathDetails.length - fileInfo.filename.length);

    return {
      name: fileInfo.filename,
      path: pathDetails,
      content: result,
      lang: FileType(lang)
    };
  }

  getFile(self, req, res) {
    self.allowed(req.user, 'getFile', 'css.fs.project.getFile');
    let path = '';
    let base = '';

    self
      ._check(req.params.id, req.query.path, (model, projectPath, basePath) => {
        path = projectPath;
        base = basePath;
        return self.fs.FileHandling.read(projectPath)
      })
      .then((result) => {
        res.json(self._readFile(result, path, base));
      })
      .catch((e) => {
        res.throw(e);
      });
  }

  putSaveFile(self, req, res) {
    self.allowed(req.user, 'putSaveFile', 'css.fs.project.putSaveFile');

    self
      ._check(req.params.id,
        req.body.path,
        (model, projectPath) => self.fs.FileHandling.save(projectPath, req.body.content))
      .then(() => res.ok("File saved"))
      .catch((e) => {
        res.throw(e);
      });
  }

  postCreateFile(self, req, res) {
    self.allowed(req.user, 'postCreateFile', 'css.fs.project.postCreateFile');

    self
      ._check(req.params.id, req.body.path, (model, projectPath, basePath) => {

        if (req.body.type && req.body.type === "folder") {
          self.fs.FolderHandling.create(projectPath)
            .then(() => res.ok("Folder created"));
        } else {
          self.fs.FileHandling.create(projectPath)
            .then(() => self.fs.FileHandling
              .read(req.body.path))
            .then((result) => {
              const data = self._readFile(result, projectPath, basePath);

              res.json(_.extend(data, {
                message: "File saved",
                status: "ok"
              }));
            });
        }
      })
      .catch((e) => {
        res.throw(e);
      });
  }

  deleteFile(self, req, res) {
    self.allowed(req.user, 'deleteFile', 'css.fs.project.deleteFile');

    self
      ._check(req.params.id, req.body.path, (model, projectPath) => {
        if (req.body.type && (req.body.type === "directory" || req.body.type === "folder")) {
          self.fs.FolderHandling.delete(projectPath);
          res.ok("Folder deleted");
        } else {
          self.fs.FileHandling.delete(projectPath);
          res.ok("File deleted");
        }
      })
      .catch((e) => {
        res.throw(e);
      });
  }

  putMove(self, req, res) {
    self.allowed(req.user, 'putMove', 'css.fs.project.putMove');

    self
      ._check(req.params.id, req.body.source, (model, source) => {
        const dest = `projects/${model.id}/${req.body.destination}`;

        return self.fs.FileHandling.move(source, dest);
      })
      .then(() => res.ok("Item moved"))
      .catch((e) => {
        res.throw(e);
      });
  }
}

module.exports = ProjectContents;
