"use strict";
const API = require("../../../engine/core/API");
const FileHandling = require("../fs/FileHandling");
const FolderHandling = require("../fs/FolderHandling");
const FileType = require("../fs/FileType");
const _ = require("lodash");

class CloneContents extends API {
  constructor(engine) {
    super(engine, "clone");
    this.fs = {
      FileHandling: new FileHandling(engine),
      FolderHandling: new FolderHandling(engine)
    };
    this.CloneRepository = engine.modules.get('css.app.models.api').CloneRepository;
  }

  register() {
    this.router.get('/:id/tree', (req, res) => this.getFileTree(this, req, res));
    this.router.get('/:id/listing', (req, res) => this.getListing(this, req, res));
    this.router.get('/:id/read', (req, res) => this.getFile(this, req, res));
    this.router.put('/:id/save', (req, res) => this.putSaveFile(this, req, res));
    this.router.post('/:id/create', (req, res) => this.postCreateFile(this, req, res));
    this.router.delete('/:id/delete', (req, res) => this.deleteFile(this, req, res));
    this.router.put('/:id/move', (req, res) => this.putMove(this, req, res));
  }

  _check(id, path, callback) {
    const join = {
      project: true,
      user: true
    };

    return this
      .CloneRepository
      .getJoin(id, join)
      .then((model) => {
        if (!model) {
          throw new Error("Model not found");
        }

        const clonePath = `clones/${model.user.id}/${model.id}/${path}`;
        const basePath = `clones/${model.user.id}/${model.id}`;
        return callback(model, clonePath, basePath);
      });
  }

  getFileTree(self, req, res) {
    self.allowed(req.user, 'getFileTree', 'css.fs.clone.getFileTree');

    self
      ._check(req.params.id, req.query.path, (model, clonePath) => {
        res.json(self.fs.FolderHandling.readTree(clonePath));
      })
      .catch((e) => {
        res.throw(e);
      });
  }

  getListing(self, req, res) {
    self.allowed(req.user, 'getListing', 'css.fs.clone.getListing');

    self
      ._check(req.params.id, req.query.path, (model, clonePath) => {
        res.json(self.fs.FolderHandling.read(clonePath));
      })
      .catch((e) => {
        res.throw(e);
      });
  }

  _readFile(result, path) {
    if (typeof(result) === "object") {
      result = result.toString("base64");
    }

    const fileInfo = this.fs.FileHandling.pathinfo(path);
    let lang = fileInfo.extname || ".txt";
    if (lang.length !== 0) {
      lang = lang.substring(1);
    }

    let pathDetails = path;
    pathDetails = pathDetails.substring(0, pathDetails.length - fileInfo.filename.length);

    return {
      name: fileInfo.filename,
      path: pathDetails,
      content: result,
      lang: FileType(lang)
    };
  }

  getFile(self, req, res) {
    self.allowed(req.user, 'getFile', 'css.fs.clone.getFile');
    let path = '';
    let base = '';

    self
      ._check(req.params.id, req.query.path, (model, clonePath, basePath) => {
        path = clonePath;
        base = basePath;
        return self.fs.FileHandling.read(clonePath);
      })
      .then((result) => {
        res.json(self._readFile(result, path, base));
      })
      .catch((e) => {
        res.throw(e);
      });
  }

  putSaveFile(self, req, res) {
    self.allowed(req.user, 'putSaveFile', 'css.fs.clone.putSaveFile');

    self
      ._check(req.params.id,
        req.body.path,
        (model, clonePath) => self.fs.FileHandling.save(clonePath, req.body.content))
      .then(() => res.ok("File saved"))
      .catch((e) => {
        res.throw(e);
      });
  }

  postCreateFile(self, req, res) {
    self.allowed(req.user, 'postCreateFile', 'css.fs.clone.postCreateFile');

    self
      ._check(req.params.id, req.body.path, (model, clonePath, basePath) => {
        if (req.body.type && req.body.type === "folder") {
          return self.fs.FolderHandling.create(clonePath)
            .then(() => res.ok("Folder created"));
        } else {
          return self.fs.FileHandling.create(clonePath)
            .then(() => self.fs.FileHandling
              .read(req.body.path))
            .then((result) => {
              const data = self._readFile(result, clonePath, basePath);

              res.json(_.extend(data, {
                message: "File saved",
                status: "ok"
              }));
            });
        }
      })
      .catch((e) => {
        res.throw(e);
      });
  }

  deleteFile(self, req, res) {
    self.allowed(req.user, 'deleteFile', 'css.fs.clone.deleteFile');

    self
      ._check(req.params.id, req.body.path, (model, clonePath) => {
        if (req.body.type && (req.body.type === "directory" || req.body.type === "folder")) {
          self.fs.FolderHandling.delete(clonePath);
          res.ok("Folder deleted");
        } else {
          self.fs.FileHandling.delete(clonePath);
          res.ok("File deleted");
        }
      })
      .catch((e) => {
        res.throw(e);
      });
  }

  putMove(self, req, res) {
    self.allowed(req.user, 'putMove', 'css.fs.clone.putMove');

    self
      ._check(req.params.id, req.body.source, (model, source) => {
        const dest = `clones/${model.user.id}/${model.id}/${req.body.destination}`;

        return self.fs.FileHandling.move(source, dest);
      })
      .then(() => res.ok("Item moved"))
      .catch((e) => {
        res.throw(e);
      });
  }
}

module.exports = CloneContents;
