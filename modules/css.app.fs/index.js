"use strict";

const FileHandling = require("./fs/FileHandling");
const FolderHandling = require("./fs/FolderHandling");

module.exports = function FS(engine) {
  const self = {};
  self.FileHandling = new FileHandling(engine);
  self.FolderHandling = new FolderHandling(engine);

  engine.permissions.import(require('./permissions.json'));
  engine.routes.build(__dirname, 'routes');

  const Project = engine.models.Project;
  const Clone = engine.models.Clone;

  Project.post("save", function ProjectFS(next) {
    if (!self.FolderHandling.exists(`projects/${this.id}`)) {
      self.FolderHandling
        .create(`projects/${this.id}`)
        .then(() => next())
        .catch((e) => next(e));
    } else {
      next();
    }
  });

  Project.post("delete", function ProjectDelete(next) {
    if (self.FolderHandling.exists(`projects/${this.id}`)) {
      self.FolderHandling.delete(`projects/${this.id}`);
    }
    next();
  });

  Clone.post("delete", function CloneDelete(next) {
    if (self.FolderHandling.exists(`clones/${this.userID}/${this.id}`)) {
      self.FolderHandling.delete(`clones/${this.userID}/${this.id}`);
    }
    next();
  });

  return self;
};
