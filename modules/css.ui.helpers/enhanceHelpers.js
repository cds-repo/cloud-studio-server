"use strict";
const crypto = require('crypto');

module.exports = function enhanceHelper(handlebars) {
  handlebars.registerHelper('hashMail',
    (context) => crypto.createHash('md5').update(context).digest('hex'));

  handlebars.registerHelper('enhanceActivity', (activity) => {
    let result = activity.activity;
    const theActivity = activity.activity.split('|');
    const md5email = crypto.createHash('md5').update(activity.user.email).digest('hex');

    switch (theActivity[0]) {
      case 'PROJECT':
        switch (theActivity[1]) {
          case 'NEW':
            result = `The project <strong>[${activity.project.name}]</strong> has been created
            by <strong>${activity.user.username}</strong>.`;
            break;
          case 'UPDATE':
          case 'UPD':
            result = `The project <strong>[${activity.project.name}]</strong> has been updated
            by <strong>${activity.user.username}</strong>.`;
            break;
        }
        break;
      case 'CLONE':
        switch (theActivity[1]) {
          case 'NEW':
            result = `A clone for contributor <strong>${activity.user.username}</strong> has been created.`;
            break;
          case 'UPDATE':
          case 'UPD':
            result = `The clone for contributor <strong>[${activity.user.username}]</strong> has been updated.`;
            break;
          case 'DELETE':
          case 'DLT':
            result = `The clone for contributor <strong>${activity.user.username}</strong> has been deleted.`;
            break;
        }
        break;
      case 'MEMBER':
        switch (theActivity[1]) {
          case 'NEW':
            result = `User <strong>${activity.user.username}</strong> has been added to the attached team.`;
            break;
          case 'DELETE':
          case 'DLT':
            result = `User <strong>${activity.user.username}</strong> has been removed from the attached team.`;
            break;
        }
        break;

    }

    return `<section class="cds-activity">
      <section class="cds-profile">
        <img src="http://www.gravatar.com/avatar/${md5email}?s=45" width="45" alt="img" class="img-circle"/>
      </section>
      <section class="cds-text">
        ${result}
      </section>
    </section>`;
  });

  handlebars.registerHelper('nl2br',
    (text) => text.replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, `$1<br>$2`));
};
