"use strict";

module.exports = function isHelper(handlebars, ranks) {
  handlebars.registerHelper('isAdmin', function isAdmin(context, options) {
    if (context.rank === ranks.ADMIN) {
      return options.fn(this);
    }

    return options.inverse(this);
  });
  handlebars.registerHelper('isManager', function isManager(context, options) {
    if (context.rank === ranks.MANAGER) {
      return options.fn(this);
    }

    return options.inverse(this);
  });
  handlebars.registerHelper('isLead', function isLead(context, options) {
    if (context.rank === ranks.LEAD) {
      return options.fn(this);
    }

    return options.inverse(this);
  });
  handlebars.registerHelper('isDeveloper', function isDeveloper(context, options) {
    if (context.rank === ranks.DEV) {
      return options.fn(this);
    }

    return options.inverse(this);
  });
  handlebars.registerHelper('atLeast', function atLeast(context, options) {
    const rank = options.hash.rank || "DEV";

    if (context.rank >= ranks[rank]) {
      return options.fn(this);
    }

    return options.inverse(this);
  });
  handlebars.registerHelper('getRank', (context) => {
    if (typeof context === "object") {
      return ranks.toString(context.rank);
    }

    return ranks.toString(context);
  });

  handlebars.registerHelper('ifEq', function ifEq(a, b, options) {
    if (a === b) {
      return options.fn(this);
    }

    return options.inverse(this);
  });

  handlebars.registerHelper('ifNotEq', function ifNotEq(a, b, options) {
    if (a !== b) {
      return options.fn(this);
    }

    return options.inverse(this);
  });

  handlebars.registerHelper('ifNot', function ifNot(cond, options) {
    if (!cond) {
      return options.fn(this);
    }

    return options.inverse(this);
  });

  handlebars.registerHelper('canEdit', function canEdit(id, user, options) {
    if (user.isAdmin() || user.id === id) {
      return options.fn(this);
    }

    return options.inverse(this);
  });

  handlebars.registerHelper('isAllowed', function isAllowed(permission, user, options) {
    if (user.isAdmin() || user.acl.has(permission)) {
      return options.fn(this);
    }

    return options.inverse(this);
  });

  handlebars.registerHelper('allowedEdit', function canEdit(id, user, permission, options) {
    if (user.isAdmin()) {
      return options.fn(this);
    }
    if (user.id === id && user.acl.has(permission)) {
      return options.fn(this);
    }

    return options.inverse(this);
  });
};
