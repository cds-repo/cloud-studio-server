"use strict";
const isHelpers = require('./isHelpers');
const enhanceHelpers = require('./enhanceHelpers');

const DashboardView = function SampleView(engine) {
  isHelpers(engine.views.handlebars, engine.models.UserRank);
  enhanceHelpers(engine.views.handlebars, engine.models.UserRank);
};


module.exports = DashboardView;
