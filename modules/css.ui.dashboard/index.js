"use strict";
const DashboardSocket = require("./sockets");

const DashboardView = function SampleView(engine) {
  const self = {};

  engine.routes.build(__dirname);
  engine.views.registerView(__dirname);
  engine.permissions.import(require('./permissions.json'));
  self.sockets = (socketEngine, socket, broadcast) => new DashboardSocket(socketEngine, socket, broadcast);

  return self;
};


module.exports = DashboardView;
