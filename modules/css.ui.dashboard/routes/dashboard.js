"use strict";
const Route = require("../../../engine/core/Route");

class Dashboard extends Route {
  constructor(engine) {
    super(engine, 'dashboard');
  }

  register() {
    this.router.get('/', (req, res) => this.getDashboard(this, req, res));
  }

  getDashboard(self, req, res) {
    self.redirectUnauthenticated(req.user, res);

    res.render('dashboard/index', {
      user: req.user,
      projectPrefix: !req.user.isAdmin() ? 'My' : 'Latest'
    });
  }
}

module.exports = Dashboard;
