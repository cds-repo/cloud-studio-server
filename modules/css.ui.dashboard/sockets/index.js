"use strict";
const Socket = require("../../../engine/core/Socket");
const _ = require('lodash');

class DashboardSocket extends Socket {
  init() {
    this.path = 'css.ui.dashboard';
  }

  build() {
    this.register('getActiveUsers', this.getActiveUser);
    this.register('getProjects', this.getProjects);
  }

  getActiveUser(self, req, data) {
    const ActiveUsers = self.engine.app.ActiveUsers;
    const users = _.map(ActiveUsers, (au) => au.user);

    self.socket.emit(self.emit('getActiveUsers'), users);
  }

  getProjects(self, req, data) {
    const Project = self.engine.models.Project;
    const Clone = self.engine.models.Clone;

    let dataFetch;

    if (req.user.isAdmin()) {
      dataFetch = Project
        .getJoin({
          type: true,
          owner: true,
          team: true,
          clones: true
        })
        .limit(10)
        .orderBy('createdAt')
        .run();
    } else {
      dataFetch = Clone
        .filter({
          userID: req.user.id
        })
        .run()
        .then((clones) => {
          const projectIds = [];

          _.each(clones, (v) => {
            projectIds.push(v.projectID);
          });

          const result = Project.getAll.apply(Project, projectIds);
          return result
            .getJoin({
              type: true,
              owner: true,
              team: true,
              clones: true
            })
            .limit(10)
            .orderBy('createdAt')
            .run();
        });
    }

    dataFetch
      .then((clones) => {
        _.each(clones, (v) => {
          v.totalClones = v.clones.length;
        });
        return clones;
      })
      .then((clones) => self.socket.emit(self.emit('getProjects'), clones))
      .catch((e) => {
        self.socket.emit('css.app.errors', e);
      });
  }
}

module.exports = DashboardSocket;
