"use strict";
const session = require('express-session');
const cookieParser = require('cookie-parser');
const RDBStore = require('express-session-rethinkdb')(session);
const _ = require("lodash");

const SessionDB = function SessionDB(engine) {
  const self = {};
  const sessionStore = new RDBStore({
    connectOptions: {
      servers: [
        {
          host: engine.config.db.host,
          port: engine.config.db.port
        }
      ],
      db: engine.config.db.db,
      discovery: false,
      pool: true,
      buffer: 50,
      max: 1000,
      timeout: 20,
      timeoutError: 1000
    },
    table: engine.config.session.table || "Sessions",
    sessionTimeout: 86400000,
    flushInterval: 60000
  });

  engine.app.use(session({
    cookieParser: cookieParser(),
    key: engine.config.session.key,
    secret: engine.config.session.secret,
    store: sessionStore,
    resave: false,
    saveUninitialized: true
  }));

  engine.sessionStore = sessionStore;

  return _.extend(self, this);
};


module.exports = SessionDB;
