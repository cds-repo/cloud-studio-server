"use strict";

class FSRepo {
  constructor(projectClone, engine) {
    this.projectClone = projectClone;
    this.engine = engine;
    this.fs = engine.modules.get('css.app.fs');
    if (this.projectClone.user) {
      this.clonePath = `clones/${this.projectClone.user.id}/${this.projectClone.id}`;
    } else {
      this.clonePath = `clones/${this.projectClone.userID}/${this.projectClone.id}`;
    }
  }

  init() {
    throw new Error("Not implemented");
  }

  read() {
    throw new Error("Not implemented");
  }

  clone() {
    throw new Error("Not implemented");
  }

  copyFromOrigin() {
    this.clone();
  }

  copyToOrigin() {
    this.forceChanges();
  }

  forceChanges() {
    throw new Error("Not implemented");
  }

  checkout(branch) {
    // Get a specific branch from a project
    throw new Error("Not implemented");
  }

  pull() {
    // Fetch the changes from "Central"
    throw new Error("Not implemented");
  }

  push() {
    // Push the changes from "Local" to "Central"
    throw new Error("Not implemented");
  }

  add(path) {
    throw new Error("Not implemented");
  }

  remove(path) {
    throw new Error("Not implemented");
  }

  commit(message) {
    throw new Error("Not implemented");
  }

  addChange(file) {
    throw new Error("Not implemented");
  }

  merge(source, destination, result) {
    throw new Error("Not implemented");
  }

  addChanges() {
    throw new Error("Not implemented");
  }
}

module.exports = FSRepo;
