"use strict";
const Route = require("../../../engine/core/Route");

class ProjectType extends Route {
  constructor(engine) {
    super(engine, 'types');
  }

  register() {
    this.router.get('/', (req, res) => this.getList(this, req, res));
    this.router.get('/create', (req, res) => this.getNew(this, req, res));
    this.router.get('/:id', (req, res) => this.getView(this, req, res));
    this.router.get('/:id/edit', (req, res) => this.getEdit(this, req, res));
    this.router.get('/:id/projects', (req, res) => this.getProjects(this, req, res));
  }

  getList(self, req, res) {
    self.allowedOrRedirect(req.user, "css.ui.projectType.list", res);

    const ProjectTypeRepo = self.engine.modules.get("css.app.models.api").ProjectTypeRepository;

    ProjectTypeRepo
      .getAllJoin({
        projects: true
      })
      .then((types) => res.render('type/index', {
        user: req.user,
        types
      }))
      .catch((e) => res.throw(e));
  }

  getView(self, req, res) {
    self.allowedOrRedirect(req.user, "css.ui.projectType.view", res);
    const ProjectTypeRepo = self.engine.modules.get("css.app.models.api").ProjectTypeRepository;

    ProjectTypeRepo
      .get(req.params.id)
      .then((type) => {
        res.render('type/view', {
          user: req.user,
          type
        });
      })
      .catch((e) => res.throw(e));
  }

  getEdit(self, req, res) {
    self.allowedOrRedirect(req.user, "css.ui.projectType.edit", res);
    const ProjectTypeRepo = self.engine.modules.get("css.app.models.api").ProjectTypeRepository;

    ProjectTypeRepo
      .get(req.params.id)
      .then((type) => {
        res.render('type/edit', {
          user: req.user,
          type
        });
      })
      .catch((e) => res.throw(e));
  }

  getNew(self, req, res) {
    self.allowedOrRedirect(req.user, "css.ui.projectType.new", res);
    const ProjectTypeModel = self.engine.models.ProjectType;

    res.render('type/edit', {
      user: req.user,
      type: new ProjectTypeModel({})
    });
  }

  getProjects(self, req, res) {
    self.allowedOrRedirect(req.user, "css.ui.projectType.projects", res);

    const Project = self.engine.modules.get("css.app.models.api").ProjectRepository;

    Project
      .filterJoin({
        typeID: req.params.id
      }, {
        type: true,
        owner: true,
        clones: true
      })
      .then((projects) => res.render('project/index', {
        user: req.user,
        projects
      }))
      .catch((e) => res.throw(e));
  }

}

module.exports = ProjectType;
