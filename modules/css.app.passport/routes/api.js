"use strict";
const express = require('express');
const _ = require("lodash");

module.exports = function api(app, engine) {
  const router = express.Router();
  const ModelsAPI = engine.modules.get("css.app.models.api");
  const UserRepository = ModelsAPI.UserRepository;
  const Configuration = engine.models.Configuration;
  const ValidationException = ModelsAPI.Exceptions.ValidationException;

  router.post('/login',
    engine.passport.authenticate('local'),
    (req, res) => {
      res.json(req._passport);
    });

  router.get('/logout',
    (req, res) => {
      if (!req.user) {
        res.error("Error", "Page not found!", 404);
        return;
      }

      req.logout();
      res.redirect('/');
    });

  router.get('/getSession', (req, res) => {
    if (req.cookies[engine.config.session.key]) {
      const sid = req.cookies[engine.config.session.key].substr(2).split('.');
      res.json({
        sid: sid[0]
      });
    } else {
      res.json({});
    }
  });

  router.post('/register', (req, res) => {
    if (req.user) {
      res.error("Error", "Page not found!", 404);
      return;
    }

    if (!engine.config.passport.register) {
      res.error(
        "RegistrationDisabled",
        "Registration has been disabled",
        404
      );
      return;
    }

    if (!req.body) {
      throw new ValidationException("No data has been sent to the application.", 400);
    }

    const userReq = req.body;
    if (!userReq) {
      throw new ValidationException("No user has been specified", 400);
    }

    const user = {
      username: userReq.username,
      firstName: userReq.firstName,
      lastName: userReq.lastName,
      password: userReq.password,
      confirmPassword: userReq.confirmPassword,
      email: userReq.email
    };

    Configuration
      .filter({
        key: "cds.permissions.default.user"
      })
      .run()
      .then((configs) => {
        let config = configs.pop();
        user.permissions = [];
        if (_.isObject(config) && _.isString(config.value) && config.value.length !== 0) {
          config = JSON.parse(config.value);
          _.forEach(config, (value) => {
            user.permissions.push({
              permission: value
            });
          });
        }
        return UserRepository
          .createJoin(user, {
            permissions: true
          });
      })
      .then(() => {
        res.ok("Registration done!");
      })
      .catch((error) => {
        engine.log.error(error.stack);
        res.throw(error);
      });
  });

  app.use('/auth', router);
};
