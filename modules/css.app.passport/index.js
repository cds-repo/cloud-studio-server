"use strict";
const passport = require('passport');
const LocalStrategy = require('passport-local');
const passportSocketIo = require('passport.socketio');
const bcrypt = require('bcryptjs');
const _ = require("lodash");

const PassportAuth = function PassportAuth(engine) {
  const self = {};
  const app = engine.app;
  const UserRepository = engine.modules.get("css.app.models.api").UserRepository;
  const ActiveUsers = engine.app.ActiveUsers = {};

  app.use(passport.initialize());
  app.use(passport.session());

  passport.serializeUser((user, done) => {
    done(null, user.id);
  });

  // This is how a user gets deserialized
  passport.deserializeUser((id, done) => {
    UserRepository
      .getJoin(id, {
        permissions: true
      })
      .then((user) => {
        user.acl = new engine.ACL(engine.permissions.all(), user.permissions);
        return done(null, user);
      })
      .catch((e) => done(null, {}));
  });


  passport.use(new LocalStrategy({
    usernameField: 'username',
    session: true
  }, (username, password, done) => {
    UserRepository
      .filterJoin({
        username
      }, {
        permissions: true
      })
      .then((results) => {
        if (results.length === 0 || bcrypt.compareSync(password, results[0].password) === false) {
          return done(null, false, {
            error: 'Incorrect username or password.'
          });
        } else {
          const user = results.pop();
          user.acl = new engine.ACL(engine.permissions.all(), user.permissions);

          return done(null, user);
        }
      });
  }));

  engine.io.use(passportSocketIo.authorize({
    cookieParser: require('cookie-parser'),
    key: engine.config.session.key,
    secret: engine.config.session.secret,
    store: engine.sessionStore
  }));

  engine.passport = passport;

  engine.routes.register(__dirname, 'routes');
  self.sockets = (socketEngine, socket, broadcast) => {
    const user = socket.request.user;
    if (!user) {
      return;
    }
    const userSend = _.cloneDeep(user);
    delete userSend.password;

    if (Object.keys(ActiveUsers).indexOf(userSend.id) < 0) {
      ActiveUsers[userSend.id] = {
        user: userSend,
        count: 0
      };
    }

    ActiveUsers[userSend.id].count++;

    broadcast.emit('css.app.login', userSend);
    socketEngine.log.debug('css.app.login');

    socket.on('disconnect', () => {
      socketEngine.log.debug('css.app.logout');
      broadcast.emit("css.app.logout", userSend.id);

      ActiveUsers[userSend.id].count--;

      if (ActiveUsers[userSend.id].count === 0) {
        delete ActiveUsers[userSend.id];
      }
    });
  };

  return _.extend(self, this);
};


module.exports = PassportAuth;
