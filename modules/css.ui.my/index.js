"use strict";

const ProjectView = function UserView(engine) {
  const self = {};

  engine.routes.build(__dirname);
  engine.permissions.import(require('./permissions.json'));

  return self;
};


module.exports = ProjectView;
