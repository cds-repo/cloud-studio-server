"use strict";
const Route = require("../../../engine/core/Route");
const _ = require("lodash");
let r;

class My extends Route {
  constructor(engine) {
    super(engine, 'my');
    r = engine.app.r;
  }

  register() {
    this.router.get('/projects', (req, res) => this.getProjects(this, req, res));
    this.router.get('/clones', (req, res) => this.getClones(this, req, res));
  }

  getProjects(self, req, res) {
    self.allowedOrRedirect(req.user, "css.ui.my.projects", res);

    const ProjectModel = self.engine.models.Project;

    const data = {
      user: req.user
    };

    ProjectModel
      .filter({
        ownerID: req.user.id
      })
      .getJoin({
        type: true,
        owner: true,
        team: true,
        clones: true
      })
      .orderBy('createdAt')
      .run()
      .then((projects) => res.render('project/index', _.extend(data, {
        projects
      })))
      .catch((e) => res.throw(e));
  }

  getClones(self, req, res) {
    self.allowedOrRedirect(req.user, "css.ui.my.clones", res);

    const Project = self.engine.models.Project;
    const Clone = self.engine.models.Clone;

    Clone
      .filter({
        userID: req.user.id
      })
      .run()
      .then((clones) => {
        const projectIds = [];

        _.each(clones, (v) => {
          projectIds.push(v.projectID);
        });

        if (projectIds.length) {
          const result = Project.getAll.apply(Project, projectIds);
          return result
            .getJoin({
              type: true,
              owner: true,
              team: true,
              clones: true
            })
            .orderBy('createdAt')
            .run();
        }
        return [];
      })
      .then((projects) => res.render('project/index', {
        clones: true,
        user: req.user,
        projects
      }))
      .catch((e) => res.throw(e));
  }

}

module.exports = My;
