"use strict";

const CDSFS = require("./CDSFS");

module.exports = function CDSFSRepo(engine) {
  const self = {};

  engine.permissions.import(require('./permissions.json'));
  engine.routes.build(__dirname, 'routes');
  const fs = engine.modules.get("css.app.fs");

  const Clone = engine.models.Clone;
  const Project = engine.models.Project;

  Clone.pre("save", function ClonePreFS(next) {
    const id = this.project ? this.project.id : this.projectID;
    if (!fs.FolderHandling.exists(`projects/${id}`)) {
      fs.FolderHandling
        .create(`projects/${id}`)
        .then(() => next())
        .catch((e) => next(e));
    }else{
      next();
    }
  });

  Clone.post("save", function CloneFS(next) {
    if (fs.FolderHandling.exists(`clones/${this.userID}/${this.id}`)) {
      next();
    }

    if (!this.project) {
      Project
        .get(this.projectID)
        .run()
        .then((project) => {
          if (project.repositoryType !== 'local') {
            return [];
          }
          this.project = project;
          const clone = self.initRepository(this);
          return clone
            .clone();
        })
        .then(() => next())
        .catch((e) => next(e));
    } else if (this.project.repositoryType !== 'local') {
      next();
    } else {
      const clone = self.initRepository(this);
      clone
        .clone()
        .then(() => next())
        .catch((e) => next(e));
    }
  });

  self.openRepository = function openRepository(clone) {
    const repository = new CDSFS(clone, engine);
    repository.read();

    return repository;
  };

  self.initRepository = function initRepository(clone) {
    const repository = new CDSFS(clone, engine);
    repository.init();

    return repository;
  };

  return self;
};
