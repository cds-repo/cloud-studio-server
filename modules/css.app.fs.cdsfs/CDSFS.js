"use strict";

const FSRepo = require("../css.app.fs.repository/FSRepo");
const _ = require("lodash");
const dirCompare = require("dir-compare");

class CDSFS extends FSRepo {
  constructor(projectClone, engine) {
    super(projectClone, engine);
    this.RepositoryInfo = require("./default.json");
  }

  init() {
    if (!this.fs.FolderHandling.exists(this.clonePath)) {
      return this.fs.FolderHandling
        .create(this.clonePath)
        .then(() => {
          this.RepositoryInfo.name = this.projectClone.project.name;
          this.RepositoryInfo.origin = `projects/${this.projectClone.project.id}`;

          return this.fs.FileHandling
            .save(`${this.clonePath}/.cdsfs`, JSON.stringify(this.RepositoryInfo));
        });
    }

    if (!this.fs.FolderHandling.exists(`${this.clonePath}/.cdsfs`)) {
      this.RepositoryInfo.name = this.projectClone.project.name;
      this.RepositoryInfo.origin = `projects/${this.projectClone.project.id}`;

      return this.fs.FileHandling
        .save(`${this.clonePath}/.cdsfs`, JSON.stringify(this.RepositoryInfo));
    }

    return this.read();
  }

  read() {
    return this.fs.FileHandling
      .read(`${this.clonePath}/.cdsfs`)
      .then((results) => {
        this.RepositoryInfo = _.extend(this.RepositoryInfo, JSON.parse(results));
      });
  }

  _copy(source, destination) {
    return this.fs.FolderHandling
      .create(destination)
      .then(() => this.fs.FolderHandling
        .copy(source, destination));
  }

  clone() {
    return this._copy(this.RepositoryInfo.origin || `projects/${this.projectClone.project.id}`,
      this.clonePath);
  }

  forceChanges() {
    return this._copy(this.clonePath,
      this.RepositoryInfo.origin || `projects/${this.projectClone.project.id}`);
  }

  _sync(source, destination) {
    const sourcePath = this.fs.FolderHandling.fullPath(source);
    const destinationPath = this.fs.FolderHandling.fullPath(destination);

    return dirCompare
      .compareSync(sourcePath, destinationPath, {
        compareSize: true
      })
      .diffSet
      .forEach((entry) => {
        // TODO: give option to merge things
        if (entry.type2 === "missing") {
          let basePath = `${destination}/${entry.relativePath}`;
          basePath = basePath.replace(/\/\//gi, '/');

          let fullPath = `${basePath}/${entry.name1}`;
          fullPath = fullPath.replace(/\/\//gi, '/');

          let sourceFullPath = `${source}/${entry.relativePath}/${entry.name1}`;
          sourceFullPath = sourceFullPath.replace(/\/\//gi, '/');

          this.fs.FolderHandling
            .create(basePath)
            .then(() => {
              if (entry.type1 === "directory") {
                return this.fs.FolderHandling.create(fullPath);
              }

              return this.fs.FileHandling.copy(sourceFullPath, fullPath);
            });
        }
      });
  }

  pull() {
    return this._sync(this.RepositoryInfo.origin, this.clonePath);
  }

  push() {
    return this._sync(this.clonePath, this.RepositoryInfo.origin);
  }
}

module.exports = CDSFS;
