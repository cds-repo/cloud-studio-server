"use strict";
const API = require("../../../engine/core/API");
const CDSFS = require("../CDSFS");

class CloneFSRepository extends API {
  constructor(engine) {
    super(engine, "clone");
    this.CloneRepository = engine.modules.get('css.app.models.api').CloneRepository;
  }

  _openRepository(clone) {
    const repository = new CDSFS(clone, this.engine);

    return repository;
  }

  _isRepositoryOk(repositoryType) {
    return repositoryType === "local";
  }

  register() {
    this.router.put('/:id/cdsfs/sync', (req, res) => this.putSync(this, req, res));
    this.router.put('/:id/cdsfs/copy', (req, res) => this.putCopy(this, req, res));
    this.router.post('/:id/cdsfs/sync', (req, res) => this.postSync(this, req, res));
    this.router.post('/:id/cdsfs/copy', (req, res) => this.postCopy(this, req, res));
  }

  _check(id, callback) {
    const join = {
      project: true,
      user: true
    };

    return this
      .CloneRepository
      .getJoin(id, join)
      .then((model) => {
        if (!model) {
          throw new Error("Model not found");
        }
        if (!this._isRepositoryOk(model.project.repositoryType)) {
          throw new Error("Invalid repository type");
        }

        return callback(model);
      });
  }

  putSync(self, req, res) {
    self.allowed(req.user, 'putSync', 'css.fs.cdsfs.putSync');

    self
      ._check(req.params.id, (model) => {
        const repofs = self._openRepository(model);
        return repofs
          .read()
          .then(() => repofs.pull())
          .then(() => res.ok("Sync done"));
      })
      .catch((e) => {
        res.throw(e);
      });
  }

  putCopy(self, req, res) {
    self.allowed(req.user, 'putSync', 'css.fs.cdsfs.putCopy');

    self
      ._check(req.params.id, (model) => {
        const repofs = self._openRepository(model);
        return repofs
          .read()
          .then(() => repofs.copyFromOrigin())
          .then(() => res.ok("Copy done"));
      })
      .catch((e) => {
        res.throw(e);
      });
  }

  postSync(self, req, res) {
    self.allowed(req.user, 'postSync', 'css.fs.cdsfs.postSync');

    self
      ._check(req.params.id, (model) => {
        const repofs = self._openRepository(model);
        return repofs
          .read()
          .then(() => repofs.push())
          .then(() => res.ok("Sync done"));
      })
      .catch((e) => {
        res.throw(e);
      });
  }

  postCopy(self, req, res) {
    self.allowed(req.user, 'putSync', 'css.fs.cdsfs.postCopy');

    self
      ._check(req.params.id, (model) => {
        const repofs = self._openRepository(model);
        return repofs
          .read()
          .then(() => repofs.copyToOrigin())
          .then(() => res.ok("Copy done"));
      })
      .catch((e) => {
        res.throw(e);
      });
  }

}

module.exports = CloneFSRepository;
