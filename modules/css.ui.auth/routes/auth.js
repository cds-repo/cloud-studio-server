"use strict";
const Route = require("../../../engine/core/Route");

class Auth extends Route {
  constructor(engine) {
    super(engine, 'auth');

    this.app.get('/', (req, res) => {
      if (this.isAuthenticated(req.user)) {
        res.redirect('/dashboard');
        return;
      }
      res.redirect('/auth/login');
    });
  }

  register() {
    this.router.get('/', (req, res) => res.redirect('/auth/login'));
    this.router.get('/login', (req, res) => this.getLogin(this, req, res));
    this.router.get('/register', (req, res) => this.getRegister(this, req, res));
  }

  getLogin(self, req, res) {
    if (self.isAuthenticated(req.user)) {
      res.redirect('/dashboard');
      return;
    }

    res.render('auth/login', {
      _csrf: ''
    });
  }

  getRegister(self, req, res) {
    if (self.isAuthenticated(req.user)) {
      res.redirect('/dashboard');
      return;
    }

    res.render('auth/register', {
      _csrf: ''
    });
  }
}

module.exports = Auth;
