"use strict";

const AuthView = function SampleView(engine) {
  engine.routes.build(__dirname);
  engine.views.registerView(__dirname);
};


module.exports = AuthView;
